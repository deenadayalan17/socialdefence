<?php
 class Organization extends CI_Controller{
   public function __construct(){
     parent::__construct();
      $this->load->helper('url');
   }
   public function organization_structure(){
     $this->load->view('templates/header');
     $this->load->view('templates/navigation');
     $this->load->view('pages/organization_structure');
     $this->load->view('templates/footer');
   }
   public function history(){
     $this->load->view('templates/header');
     $this->load->view('templates/navigation');
     $this->load->view('pages/history');
     $this->load->view('templates/footer');
   }
 }

<?php
class CitizenCharter extends CI_Controller{

  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
  }

  public function index(){
    $this->load->view('templates/header');
    $this->load->view('templates/navigation');
    $this->load->view('pages/citizen_charter');
    $this->load->view('templates/footer');
  }
}

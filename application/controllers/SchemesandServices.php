<?php
class SchemesandServices extends CI_Controller{

  public function __construct(){
    parent::__construct();
  }

  public function ujjawala_scheme(){
    $this->load->view('templates/header');
    $this->load->view('templates/navigation');
    $this->load->view('pages/ujjawala_scheme');
    $this->load->view('templates/footer');
  }
}

<?php
class Pages extends CI_Controller{

  public function __construct(){
    parent::__construct();
  }

  public function views($page='home'){
    if(!file_exists(APPPATH."views/pages/$page".'.php')):
      show_404();
    else:
      $title = ucwords(str_replace("_"," ",$page));
      $this->load->view('templates/header',['title'=>$title]);
      $this->load->view('templates/navigation');
      $this->load->view("pages/$page");
      $this->load->view('templates/footer');
    endif;
  }
}

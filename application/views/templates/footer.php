<div class="container-fluid footer1 bg-info ">
  <div class="container-fluid">
  <div class="row pt-2">
    <div class="col-md-4 py-3">
      <p class="footer-title">Department of Social Defence</p>
      <p class="address">Department of Social Defence<br>
        No. 300 Purasawalkam High Road <br>
        Kellys, Chennai - 600010. <br>
        044-2642 6421, 2642 7022 <br>
        dsd.tn@nic.in
      </p>
    </div>
    <div class="col-md-4 py-3">
      <p class="footer-title">Regional Office</p>
      <p class="address">No.5, 2nd Floor, “D” Block, Besant Road,<br>
        Baskar Complex, Chinna Chokhikulam,<br>
        Madurai – 625 002.<br>
        Phone No. : 0452-2529314<br>
        Email: romadurai[dot]tn[at]gmail[dot]co
      </p>
    </div>

    <div class="col-md-4 py-3">
      <p class="footer-title">State Child Protection Society</p>
      <p class="address">New No. 300, Purasawalkam High Road,<br>
        Kellys, Chennai - 600 010.<br>
        Phone No. : 044-26421358<br>
        E-mail : scpstn1@gmail.com,<br>
        FaceBook page: https://www.facebook.com/scpstn
      </p>
    </div>
  </div>
</div>
</div>
<div class="container-fluid bg-info footer2">
  <div class="container border-top py-2">
    <div class="row">
      <div class="col-md-4">
        <p>&copy 2018, Department of Social Defence</p>
      </div>
      <div class="col-md-4">
        <p class="">
          <a href="http://localhost/feedback">Feedback</a> |
          <a href="http://localhost/disclaimer">Disclaimer</a> |
          <a href="http://localhost/socialdefence/organization/history/contact_us">Contact Us</a> |
          <a href="http://localhost/sitemap">Site Map</a>
        </p>
      </div>
      <div class="col-md-4">
        <p>Desgined & Developed By: <a href="https://tnega.tn.gov.in">TNeGA</a></p>

      </div>
    </div>
  </div>
</div>



<script src="<?= base_url()?>assets/js/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
<script src="<?= base_url()?>assets/js/popper.min.js" crossorigin="anonymous"></script>
<script src="<?= base_url()?>assets/js/bootstrap.min.js" crossorigin="anonymous"></script>
<script src="<?= base_url()?>assets/js/bootstrap-navbar.js"></script>
<script src="<?= base_url()?>assets/js/jquery.flexslider-min.js"></script>
<script src="<?= base_url()?>assets/js/jquery.font-accessibility.min.js"></script>
<script src="<?= base_url()?>assets/js/store.min.js"></script>
<script src="<?= base_url()?>assets/js/rv-jquery-fontsize-2.0.3.min.js"></script>
<script>
  // Can also be used with $(document).ready()
  $(document).ready(function() {
    $('.flexslider').flexslider({
      animation: "slide",
      animationLoop: false,
      itemWidth: 150,
      itemMargin: 15,
      minItems: 2,
      maxItems: 7
    });
  });
  $.rvFontsize({
    targetSection: 'body, .list-group-item',
    store: true, // store.min.js required!
    controllers: {
        appendTo: '#rvfs-controllers',
        showResetButton: true
    }
}); 
</script>
</body>
</html>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url()?>assets/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/flexslider.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/bootstrap-navbar.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/custom-style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/rvfs.css" >
    <title><?= "$title | Department of Social Defence"; ?></title>
  </head>
  <body id="body">
    <div class="row bg-info">
      <div class="col-md-9">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="#"><i class="fa fa-phone"></i>&nbsp;&nbsp;044 - 2642 6421, 2642 7022</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"><i class="fa fa-envelope"></i>&nbsp;&nbsp;dsd@tn.nic.in</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;Reach us: No. 300, Purasawalkam High Road, Chennai -30</a>
          </li>
        </ul>
      </div>
      <div class="col-md-3 mt-1">
        <div class="btn-group" role="group" aria-label="Basic example">
          <button type="button" class="btn btn-light btn-sm decrease-me" id="decreaseFont"><i class="fa fa-font"></i>-</button>
          <button type="button" class="btn btn-light btn-sm reset-me"><i class="fa fa-font"></i></button>
          <button type="button" class="btn btn-light btn-sm increase-me"><i class="fa fa-font"></i>+</button>
        </div>
        <div class="btn-group" role="group">
          <button type="button" class="btn btn-light btn-sm"><i class="fa fa-adjust"></i></button>
          <button type="button" class="btn btn-dark btn-sm"><i class="fa fa-adjust text-white"></i></button>
        </div>
      </div>
    </div>
    <header>
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <img src="<?= base_url()?>/assets/images/logo.png" class="img-fluid" style="height:90px!important" />
          </div>
          <div class="col-md-6 pt-2">
            <img src="<?= base_url()?>assets/images/child-line.png">
              <img src="<?= base_url()?>assets/images/helpline.png">
          </div>
          <!-- <div class="col-md-2">
              <div class="btn-group mt-2" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-secondary js-font-decrease">A-</button>
                <button type="button" class="btn btn-secondary js-font-normal">A</button>
                <button type="button" class="btn btn-secondary js-font-increase">A+</button>
              </div>
            <form class="form-inline">
              <div class="form-group mb-2 mt-3 pr-2">
                <input type="text" class="form-control" id="search" value="" name="search">
              </div>
                <button type="submit" class="btn btn-primary mb-2 mt-3">Search</button>
            </form>
          </div> -->
        </div>
      </div>
    </header>

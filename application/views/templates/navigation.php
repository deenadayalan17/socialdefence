<nav class="navbar navbar-expand-lg navbar-light bg-info sticky-top">
  <div class="container">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url() ?>">Home</span></a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Organization
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?= base_url()?>organization/organization-structure">Organization Structure</a>
          <a class="dropdown-item" href="<?= base_url()?>organization/history">History</a>
          <a class="dropdown-item" href="<?= base_url()?>organization/administrative-setup">Administrative Center</a>
        </div>
      </li>
      <!-- <li class="nav-item">
        <a class="nav-link" href="<?= base_url()?>citizen-charter">Citizen Charter</a>
      </li> -->
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="https://bootstrapthemes.co" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Schemes and Services
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <li><a class="dropdown-item" href="<?= base_url()?>schemes-and-services/ujjawala-scheme">Ujjawala Scheme</a></li>
              <li><a class="dropdown-item" href="<?= base_url()?>schemes-and-services/swadhar-scheme">Swadhar Scheme</a></li>
              <li><a class="dropdown-item" href="<?= base_url()?>schemes-and-services/child-line">Child Line</a>
              <li><a class="dropdown-item dropdown-toggle" href="#">Integrated Child Protection Scheme</a>
                  <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="<?= base_url()?>schemes-and-services/icps/institutional-services">Institutional Services</a></li>
                      <li><a class="dropdown-item dropdown-toggle" href="#">Non Institutional Services</a>
                          <ul class="dropdown-menu">
                              <li><a class="dropdown-item" href="<?= base_url()?>schemes-and-services/icps/non-institutional-services/sponsership-and-foster-care">Sponsership & Foster Care</a></li>
                              <li><a class="dropdown-item" href="<?= base_url()?>schemes-and-services/icps/non-institutional-services/adoption">Adoption</a></li>
                              <li><a class="dropdown-item" href="<?= base_url()?>schemes-and-services/icps/non-institutional-services/after-care">After Care</a></li>
                          </ul>
                      </li>
                  </ul>
              </li>
              <li><a class="dropdown-item" href="<?= base_url()?>schemes-and-services/de-addiction-program">De-Addiction Program (IRCA)</a></li>
            </ul>
          </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="https://bootstrapthemes.co" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Institutions
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <li><a class="dropdown-item" href="<?= base_url()?>institutions/juvenile-justice-boards">Juvenile Justice Bords</a></li>
              <li><a class="dropdown-item" href="<?= base_url()?>institutions/child-welfare-committee">Child Welfare Committee</a></li>
              <li><a class="dropdown-item dropdown-toggle" href="#">Government Run Homes</a>
                  <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="<?= base_url()?>institutions/government/children-homes">Children Homes</a></li>
                      <li><a class="dropdown-item" href="<?= base_url()?>institutions/government/observation-homes">Observation Homes</a></li>
                      <li><a class="dropdown-item" href="<?= base_url()?>institutions/government/after-care-institutions">After Care Institutions</a></li>
                      <li><a class="dropdown-item" href="<?= base_url()?>institutions/government/special-homes">Special Homes</a></li>
                      <li><a class="dropdown-item" href="<?= base_url()?>institutions/government/reception-units">Reception Units</a></li>
                  </ul>
              </li>
              <li><a class="dropdown-item dropdown-toggle" href="#">NGO Run Homes</a>
                  <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="<?= base_url()?>institutions/ngo/children-homes">Children Homes</a></li>
                      <li><a class="dropdown-item" href="<?= base_url()?>institutions/ngo/observation-homes">Observation Homes</a></li>
                      <li><a class="dropdown-item" href="<?= base_url()?>institutions/ngo/resource-centers">Resource Centers</a></li>
                      <li><a class="dropdown-item" href="<?= base_url()?>institutions/ngo/reception-units">Reception Units</a></li>
                      <li><a class="dropdown-item" href="<?= base_url()?>institutions/ngo/de-addiction-centers">De-Addiction Centers</a></li>
                  </ul>
              </li>
              <li><a class="dropdown-item dropdown-toggle" href="#">Registration Details of CCI's</a>
                  <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="<?= base_url()?>institutions/registration/registered">Registered CCI's</a></li>
                      <li><a class="dropdown-item" href="<?= base_url()?>institutions/registration/applied-for-renewal">Applied for Renewal</a></li>
                      <li><a class="dropdown-item" href="<?= base_url()?>institutions/registration/not-applied-for-renewal">Not Applied for Renewal</a></li>
                      <li><a class="dropdown-item" href="<?= base_url()?>institutions/registration/applied-for-registration">Applied for Registration</a></li>
                      <li><a class="dropdown-item" href="<?= base_url()?>institutions/registration/not-applied-for-registration">Not Applied for Registration</a></li>
                  </ul>
              </li>
            </ul>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Documents
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="<?= base_url()?>documents/acts-and-rules">Acts and Rules</a>
              <a class="dropdown-item" href="<?= base_url()?>documents/government-orders">Government Orders</a>
              <a class="dropdown-item" href="<?= base_url()?>documents/policies">Policies</a>
              <a class="dropdown-item" href="<?= base_url()?>documents/downloads">Downloads</a>
              <a class="dropdown-item" href="<?= base_url()?>documents/notifications">Notifications</a>
            </div>
          </li>
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Publications
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?= base_url()?>publications/annual-reports">Annual Reports</a>
          <a class="dropdown-item" href="<?= base_url()?>publications/policy-notes">Policy Notes</a>
          <a class="dropdown-item" href="<?= base_url()?>publications/information-education-and-communication">Information Education & Communication</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" role="button" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Contacts</a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?= base_url()?>contacts/state-child-protection-society">State Child Protection Society</a>
          <a class="dropdown-item" href="<?= base_url()?>documents/contact/DCPU.pdf" target="_blank">District Child Protection Units</a>
        </div>
      </li>
    </ul>
  </div>
</div>
</nav>
<?php if($this->uri->segment(1)!=''): ?>  
<div class="container">
  <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <?php for($i=1;$i<=$this->uri->total_segments();$i++): ?>
            <?php if(!$i==$this->uri->total_segments()): ?>
              <li class="breadcrumb-item"><a href="#"><?= ucfirst($this->uri->segment($i)); ?></a></li>
            <?php else: ?>
              <li class="breadcrumb-item active" aria-current="page"><?= ucfirst($this->uri->segment($i));?></li>
          <?php endif; ?>
        <?php endfor; ?>
      </ol>
  </nav>
</div>
<?php endif; ?>

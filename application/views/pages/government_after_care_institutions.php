<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <section class="title">
            <h5 class="card-title pb-1 border-bottom">After Care Institutions</h5>
          </section>
          <section class="page-content">
            <p>After Care Organisations are meant for providing services for children who are discharged form the Children Homes / Special Homes and could not be restored to their family for various reasons. Two After Care Organizations are functioning for boys at Athur in Chengalpattu (Kanchipuram District) and Madurai. One After Care Organization is functioning at Vellore for women. Job oriented vocational training and skill development programme are imparted to inmates in these Homes.</p>
            <p>In addition the Bala Vihar, Chennai (a Non-Governmental Organization) is supported by the Government for running a shelter home with vocationl training exclusively or mentally challenged adult girls.</p>
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <td>1.</td>
                  <td>Government After Care Organisation,   <br /> Athur, <br /> Chengalpet – 603 101.<br /> Phone No.04114-27433903</td>
                </tr>
                <tr>
                  <td>2.</td>
                  <td>Government After Care Organisation, <br /> 1/501,Sri Nagar, Muniyandi Kovil Street,<br /> Postal Telegraph Nagar Ext., <br /> Madurai - 625 017.<br /> Phone No.0452-2520 771</td>
                </tr>
                <tr>
                  <td>3.</td>
                  <td>Government After Care Organisation for Women<br /> ‘Sun Shine’ #9, Officers Line, <br /> Vellore - 632 001 <br /> Phone No.0416-2222812</td>
                </tr>
              </tbody>
            </table>
          </section>
        </div>
      </div>
    </div>
      <div class="col-md-3">
        <?php $this->load->view('pages/sidebar'); ?>
      </div>
  </div>
</div>

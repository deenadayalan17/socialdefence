<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <section class="title">
            <h5 class="card-title pb-1 border-bottom">CCI's Not Applied for Renewal</h5>
          </section>
          <section class="page-content">
            <table class="table table-bordered" style="height: 219px;" width="948">
            <tbody>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Ariyalur.pdf" target="_blank" rel="noopener">Ariyalur</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Chennai.pdf" target="_blank" rel="noopener">Chennai</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Coimbatore.pdf" target="_blank" rel="noopener">Coimbatore</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Cuddalore.pdf" target="_blank" rel="noopener">Cuddalore</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Dharmapuri.pdf" target="_blank" rel="noopener">Dharmapurai</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Dindigul.pdf" target="_blank" rel="noopener">Dindigul</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Erode.pdf" target="_blank" rel="noopener">Erode</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Kanchipuram.pdf" target="_blank" rel="noopener">Kanchipuram</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Kanyakumari.pdf" target="_blank" rel="noopener">Kanyakumari</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Karur.pdf" target="_blank" rel="noopener">Karur</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Krishnagiri.pdf" target="_blank" rel="noopener">krishnagiri</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Madurai.pdf" target="_blank" rel="noopener">Madurai</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Nagapattinam.pdf" target="_blank" rel="noopener">Nagapattinam</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Namakkal.pdf" target="_blank" rel="noopener">Namakkal</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Perambalur.pdf" target="_blank" rel="noopener">Perambalur</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Pudukkottai.pdf" target="_blank" rel="noopener">Pudukkottai</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Ramanathapuram.pdf" target="_blank" rel="noopener">Ramanathapuram</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Salem.pdf" target="_blank" rel="noopener">Salem</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Sivagangai.pdf" target="_blank" rel="noopener">Sivaganga</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Thanjavur.pdf" target="_blank" rel="noopener">Tanjavur</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Nilgiris.pdf" target="_blank" rel="noopener">The Nilgiris</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Theni.pdf" target="_blank" rel="noopener">Theni</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Thiruvallur.pdf" target="_blank" rel="noopener">Thiruvallur</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Thiruvarur.pdf" target="_blank" rel="noopener">Thiruvarur</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Trichy.pdf" target="_blank" rel="noopener">Tiruchirappalli</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Thirunelveli.pdf" target="_blank" rel="noopener">Tirunelveli</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Tiruppur.pdf" target="_blank" rel="noopener">Tiruppur</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Tiruvannamalai.pdf" target="_blank" rel="noopener">Tiruvannamalai</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Thoothukudi.pdf" target="_blank" rel="noopener">Thoothukkudi</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Vellore.pdf" target="_blank" rel="noopener">Vellore</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Villupuram.pdf" target="_blank" rel="noopener">Villupuram</a></td>
            <td><a href="<?= base_url() ?>documents/registration/not-applied-for-renewal/Virudhunagar.pdf" target="_blank" rel="noopener">Virudhunagar</a></td>
            <td></td>
            </tr>
            </tbody>
            </table>
          </section>
        </div>
      </div>
    </div>
      <div class="col-md-3">
        <?php $this->load->view('pages/sidebar'); ?>
      </div>
    </div>
</div>

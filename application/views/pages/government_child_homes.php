<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title pb-1 border-bottom">Government Children Homes</h5>
          <p>In Tamil Nadu, Children Homes are functioning under the provisions of Juvenile Justice Act, 2000 to cater to the needs of children requiring care and protection. 10 Homes are functioning under the control of Department of Social Defence, 26 under the Department of Social Welfare and 185 Homes under the NGOs with grants-in-aid from the Government.</p>
          <p>The State Government supports Non-Governmental Organizations which run child care institutions by providing grants-in-aid of Rs.2000/- per child per month towards the maintenance of children apart from bedding, rent, water,electricity, transpotation, salary for staff and other contingencies. Children Homes under Bala Vihar (Boys and Girls) and Sree Arunodayam in Chennai and Arivagam at Mayiladuthurai are for children with special needs.</p>
          <p>The Children are provided with basic needs of food (including weekly once Mutton, weekly thrice Egg/Banana), clothing and bedding (including 4 sets of Uniforms, 2 sets of Casuals and 2 sets of Chappals) and Dormitory with adequate facilities.</p>
          <table class="table table-bordered" style="height: 1315px;" width="932">
            <tbody>
              <tr>
                <td>S. No</td>
                <td width="320">Name of the Institution</td>
                <td>Age group for admission</td>
                <td>Proposed Sanctioned Strength</td>
                <td>Districts covered</td>
              </tr>
              <tr>
                <td>1</td>
                <td>Government Children's Home for Girls,<br /> 300, Purasawalkam High Road,<br /> Chennai-600 010.<br /> Phone No.044-26421279</td>
                <td>8-18</td>
                <td>300</td>
                <td>Thanjavur, Tiruvarur, Tiruchirapalli, Vellore, Trivallur, Kancheepuram and Chennai city</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Government Children's Home for Boys,<br /> Ceylon Government Quarantine Camp,<br /> Tattaparai-623 034.<br /> Phone No. 0461-2261235</td>
                <td>6-18</td>
                <td>200</td>
                <td>Kanniyakumari, Tirunelveli, Tuticorin</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Government Children's Home for Boys,<br /> Bombay Trunk Road,<br /> Ranipet-632 402.<br /> Phone No.04172-272506.</td>
                <td>8-18</td>
                <td>300</td>
                <td>Vellore, Thiruvannamalai</td>
              </tr>
              <tr>
                <td>4</td>
                <td>Government Children's Home for Boys,<br /> V.O.C Nagar,<br /> Thanjavur-613 007.<br /> Phone No.04362-237013.</td>
                <td>8-18</td>
                <td>300</td>
                <td>Thanjavur, Pudukottai, Nagapattinam, Trichy, Thiruvarur, Karur, Perambalur</td>
              </tr>
              <tr>
                <td>5</td>
                <td>Government Children's Home for Boys,<br /> No.58, Suryanarayana Chetty Street,<br /> Royapuram,<br /> Chennai- 600 013.<br /> Phone No. 044-25951450</td>
                <td>6-14</td>
                <td>300</td>
                <td>Chennai, Tiruvallur and Kancheepuram</td>
              </tr>
              <tr>
                <td>6</td>
                <td>Government Children's Home for Boys,<br /> Mallipudur-626 141.<br /> Srivilliputhur Taluk,<br /> Virudhunagar District.<br /> Phone No.04563-281539</td>
                <td>6-18</td>
                <td>300</td>
                <td>Madurai, Virudhunagar, Theni, Dindigul, Sivagangai, Ramanathapuram</td>
              </tr>
              <tr>
                <td>7</td>
                <td>Government Children's Home for Boys,<br /> Chinnar Reservoir Project Area,<br /> Panchampalli-632 812.<br /> Dharmapuri District.<br /> Phone No.04348-237649</td>
                <td>13-18</td>
                <td>200</td>
                <td>Dharmapuri, Erode, Namakkal, Salem, Nilgiris, Coimbatore</td>
              </tr>
              <tr>
                <td>8</td>
                <td>Government Children's Home for Boys,<br /> SF-151/2A Kollukattumedu,<br /> Nanjai Oothukulli, Lakkapuram P.O.,<br /> Karur Road<br /> Erode-638 002.<br /> Phone No.0424-2401916</td>
                <td>6-12</td>
                <td>100</td>
                <td>Dharmapuri, Erode, Nilgiris, Coimbatore, Salem and Namakkal</td>
              </tr>
              <tr>
                <td>9</td>
                <td>Government Children's Home for Boys,<br /> No-1, Shanmuga Perman Road,<br /> kilperumbakkam,<br /> Villupuram-605 602.<br /> Phone No.04146-241702</td>
                <td>8-18</td>
                <td>100</td>
                <td>Villupuram and Cuddalore</td>
              </tr>
              <tr>
                <td>10</td>
                <td>Government Children's Home for Boys,<br /> 'O' Siruvayal, Karaikudi-623 208<br /> Phone No.04565-200828</td>
                <td>6-12</td>
                <td>100</td>
                <td>Ramanathapuram, Sivagangai and Pudukkottai</td>
              </tr>
            </tbody>
          </table>
          <p><b> 26 Children Homes are transferred from Social Welfare Department during 2016-17 in G.O. Ms. No.74, Social Welfare &amp; Nutritious Meal Programme (SW8(1)) Department, dt.30.11.2015</b></p>
          <table class="table table-bordered">
            <tbody>
              <tr>
                <td>S.No</td>
                <td>Name of Institution</td>
                <td>Sanctioned Strength</td>
              </tr>
              <tr>
                <td>1</td>
                <td>Government Children Home, <br /> 1,Ponnappan Street, <br /> Waltax Road, Chennai- 600 003</td>
                <td align="center">250</td>
              </tr>
<tr>
<td width="50" height="46">
<p align="center">2</p>
</td>
<td width="411">
<p>Government Children Home, <br /> 12,Sachithanantham Street, <br /> Kosapet, Chennai-600 012</p>
</td>
<td width="88">
<p align="center">250</p>
</td>
</tr>
<tr>
<td width="50" height="43">
<p align="center">3</p>
</td>
<td width="411">
<p>Government Children Home, <br /> 52,Krishnan Street,Pillaiar Palayam <br /> Kancheepuram</p>
</td>
<td width="88">
<p align="center">250</p>
</td>
</tr>
<tr>
<td width="50" height="62">
<p align="center">4</p>
</td>
<td width="411">
<p>Government Children Home <br /> Nellikuppam Road, <br /> Service Home Complex, <br /> Cuddalore</p>
</td>
<td width="88">
<p align="center">400</p>
</td>
</tr>
<tr>
<td width="50" height="62">
<p align="center">5</p>
</td>
<td width="411">
<p>Government Children Home <br /> Nellikuppam Road, <br /> Service Home Complex, <br /> Cuddalore Tsunami</p>
</td>
<td width="88">
<p align="center">100</p>
</td>
</tr>
<tr>
<td width="50" height="62">
<p align="center">6</p>
</td>
<td width="411">
<p>Government Children Home, <br /> Singamurugan Koil Back side,Sengansalai, <br /> Near Govt.College, <br /> Tiruvanamalai</p>
</td>
<td width="88">
<p align="center">250</p>
</td>
</tr>
<tr>
<td width="50" height="43">
<p align="center">7</p>
</td>
<td width="411">
<p>Government Children Home, <br /> 87,Thandupadal Street,Anna Sagaram, <br /> Dharmapuri</p>
</td>
<td width="88">
<p align="center">250</p>
</td>
</tr>
<tr>
<td width="50" height="43">
<p align="center">8</p>
</td>
<td width="411">
<p>Government Children Home, <br /> suriyam palayam, RN Puthur, <br /> Erode</p>
</td>
<td width="88">
<p align="center">250</p>
</td>
</tr>
<tr>
<td width="50" height="43">
<p align="center">9</p>
</td>
<td width="411">
<p>Government Children Home, <br /> Kodappamanthu, <br /> The Nilgiris</p>
</td>
<td width="88">
<p align="center">150</p>
</td>
</tr>
<tr>
<td width="50" height="43">
<p align="center">10</p>
</td>
<td width="411">
<p>Government Children Home, <br /> Ganapathy Hudco Colony,Peelamedu Post, <br /> Coimbatore - 641 006</p>
</td>
<td width="88">
<p align="center">250</p>
</td>
</tr>
<tr>
<td width="50" height="62">
<p align="center">11</p>
</td>
<td width="411">
<p>Government Children Home, <br /> Avur Pirivu Salai, Mathur,Pudukottai Main Road, <br /> Trichy</p>
</td>
<td width="88">
<p align="center">250</p>
</td>
</tr>
<tr>
<td width="50" height="43">
<p align="center">12</p>
</td>
<td width="411">
<p>Government Children Home <br /> Dr.Thangaraj Colony,K.K.Nagar, <br /> Madurai-20</p>
</td>
<td width="88">
<p align="center">250</p>
</td>
</tr>
<tr>
<td width="50" height="43">
<p align="center">13</p>
</td>
<td width="411">
<p>Government Children Home, <br /> Collectorate, Velunachiyar Complex, <br /> Dindigul- 624 003</p>
</td>
<td width="88">
<p align="center">250</p>
</td>
</tr>
<tr>
<td width="50" height="43">
<p align="center">14</p>
</td>
<td width="411">
<p>Government Children Home, <br /> Aranmanai Complex <br /> Ramanathapuram</p>
</td>
<td width="88">
<p align="center">250</p>
</td>
</tr>
<tr>
<td width="50" height="43">
<p align="center">15</p>
</td>
<td width="411">
<p>Government Children Home, <br /> Thattampatti Salai,Soolakarai, <br /> Virudunagar</p>
</td>
<td width="88">
<p align="center">250</p>
</td>
</tr>
<tr>
<td width="50" height="43">
<p align="center">16</p>
</td>
<td width="411">
<p>Government Children Home, <br /> Palliyur Pillai Vayal Colony, <br /> Sivagangai</p>
</td>
<td width="88">
<p align="center">200</p>
</td>
</tr>
<tr>
<td width="50" height="43">
<p align="center">17</p>
</td>
<td width="411">
<p>Government Children Home, <br /> Membalam <br /> Tanjore</p>
</td>
<td width="88">
<p align="center">250</p>
</td>
</tr>
<tr>
<td width="50" height="43">
<p align="center">18</p>
</td>
<td width="411">
<p>Government Children Home, <br /> 1836,Ambalpuram Ist Street, <br /> Pudukottai</p>
</td>
<td width="88">
<p align="center">250</p>
</td>
</tr>
<tr>
<td width="50" height="62">
<p align="center">19</p>
</td>
<td width="411">
<p>Government Children Home, <br /> No.40-A,Melakkottai,Vasal Street,Srivaikundam, <br /> Thoothukudi</p>
</td>
<td width="88">
<p align="center">200</p>
</td>
</tr>
<tr>
<td width="50" height="43">
<p align="center">20</p>
</td>
<td width="411">
<p>Government Children Home, <br /> Parnkikal Sabari Anai, <br /> Nagerkoil</p>
</td>
<td width="88">
<p align="center">250</p>
</td>
</tr>
<tr>
<td width="50" height="43">
<p align="center">21</p>
</td>
<td width="411">
<p>Government Children Home,Rajiv Gandhi Nagar,Senkuttai, Katpadi, <br /> Vellore</p>
</td>
<td width="88">
<p align="center">50</p>
</td>
</tr>
<tr>
<td width="50" height="62">
<p align="center">22</p>
</td>
<td width="411">
<p>Government Children Home, <br /> 26-A,Senguntar South Street,Ambasamuthiram, <br /> Tirunelveli</p>
</td>
<td width="88">
<p align="center">100</p>
</td>
</tr>
<tr>
<td width="50" height="31">
<p align="center">23</p>
</td>
<td width="411">
<p>Government Children Home, <br /> EastSouth Palpannaichari, <br /> Samathan Pettai, Nagapattinam</p>
</td>
<td width="88">
<p align="center">100</p>
</td>
</tr>
<tr>
<td width="50" height="58">
<p align="center">24</p>
</td>
<td width="411">
<p>Government Children Home, <br /> Tsunami Orphanage, EastSouth Palpannaichari, Samathan Pettai, <br /> Nagapattinam Nagapattinam-Tsunami</p>
</td>
<td width="88">
<p align="center">100</p>
</td>
</tr>
<tr>
<td width="50" height="25">
<p align="center">25</p>
</td>
<td width="411">
<p>Government Children Home, <br /> I.T.I.Compound,Ayyam Thirumaligai, <br /> Salem- 636 102</p>
</td>
<td width="88">
<p align="center">250</p>
</td>
</tr>
<tr>
<td width="50" height="62">
<p align="center">26</p>
</td>
<td width="411">
<p>Government Children Home, <br /> 33-18,Periya Mariamman, Kovil Street <br /> Pedanayakanpalayam, Athur, <br /> Salem - 636 102</p>
</td>
<td width="88">
<p align="center">100</p>
</td>
</tr>
<tr>
<td colspan="2" width="475" height="4">
<p align="center">Total</p>
</td>
<td width="88">
<p align="center"><a name="_GoBack"></a> 5,500</p>
</td>
</tr>
</tbody>
</table>

        </div>
      </div>
    </div>
    <div class="col-md-3">
      <?php $this->load->view('pages/sidebar'); ?>
    </div>
  </div>


</div>

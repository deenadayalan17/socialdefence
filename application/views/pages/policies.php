<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title pb-1 border-bottom">Policies</h5>
          <table class="table table-bordered">
            <tbody>
              <tr>
                <td><a href="<?= base_url()?>documents/policy/National-Plan-of-Action.pdf" target="_blank" rel="noopener">National Plan of Action for Children 2016 </a></td>
              </tr>
              <tr>
                <td><a href="<?= base_url()?>documents/policy/The-National-Policy-for-Children-2013.pdf" target="_blank" rel="noopener">National Policy for Children, 2013</a></td>
              </tr>
            </tbody>
          </table>
          <div class="my-5 py-5">

          </div>
          <div class="my-5 py-5">

          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <?php $this->load->view('pages/sidebar'); ?>
    </div>
  </div>


</div>

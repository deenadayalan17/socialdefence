<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <h3 class="card-title pb-1 border-bottom page-title">Citizen Charter</h5>
          <p class="page-content">
            <h5><strong>I. Target Groups </strong></h5>
              <p>The Department of Social Defence provides services to:</p>
                <ul>
                  <li>Neglected Children aged 11-18 yrs.</li>
                  <li>Abused Children</li>
                  <li>Children in conflict with law</li>
                  <li>Street and working Children</li>
                  <li>Stranded Girls &amp; Unmarried Mothers</li>
                  <li>Women and Girls in moral danger</li>
                  <li>Women and Girls who are found to be victims under The Immoral Traffic (Prevention) Act, 1956</li>
                  <li>Combating Trafficking and Commercial Sexual Exploitation of Women and Children</li>
                  <li>Persons with drug addiction.</li>
                </ul>
              <h5><strong>II. Services Provided</strong></h5>
                <ol>
                  <li>Rescue and provision of basic amenities such as food, clothing shelter etc., to the children in need of care and protection, children who are alleged/ proved to be in conflict with law, Women and Children - rescued victims under Immoral Traffic (Prevention ) Act,1956 and Women and Children who are in moral danger.</li>
                </ol>
                <ol start="2">
                  <li>Imparting formal and non formal education to the target groups depending on individual‟s requirement.</li>
                  <li>Provision of psycho - social services such as yoga-meditation, couselling, group councelling and guidance to parents.</li>
                  <li>Implementing rehabilitation by imparting life skill education and modernized vocational training programme.</li>
                  <li>Financial assistance to the discharged children and women.</li>
                  <li>Scheme of Marriage assistance.</li>
                  <li>Follow up on sustained income Generating Vocational training and life style changes.</li>
                  <li>Providing Legal Aid.</li>
                </ol>
                <h5><strong>III. The Acts administered by the Department of Social Defence</strong></h5>
                  <ol>
                    <li>Juvenile Justice (Care and Protection of Children)Act, 2000 as amended in 2006.</li>
                    <li>Immoral Traffic (Prevention) Act,1956.</li>
                  </ol>
                <h5><strong>IV. Various Institutions run by the Department of Social Defence</strong></h5>
                <table class="table table-bordered">
                <tbody>
                  <tr class="bg-secondary text-white">
                    <th valign="middle">S.No</th>
                    <th valign="middle">Institutions</th>
                    <th valign="middle">Government Run</th>
                    <th valign="middle">Government Aided and run by NGO’s</th>
                    <th valign="middle">Total</th>
                  </tr>
                  <tr>
                    <td>1</td>
                    <td>Reception Centres</td>
                    <td>9</td>
                    <td>14</td>
                    <td>23</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Observation Homes</td>
                    <td>6</td>
                    <td>2</td>
                    <td>8</td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>Special Homes</td>
                    <td>2</td>
                    <td>-</td>
                    <td>2</td>
                  </tr>
                  <tr>
                    <td>4</td>
                    <td>Children Homes</td>
                    <td>10</td>
                    <td>20</td>
                    <td>30</td>
                  </tr>
                  <tr>
                    <td>5</td>
                    <td>Resource Centres</td>
                    <td>-</td>
                    <td>18</td>
                    <td>31</td>
                  </tr>
                  <tr>
                    <td>6</td>
                    <td>Crisis Intervention Centres</td>
                    <td>-</td>
                    <td>1</td>
                    <td>1</td>
                  </tr>
                  <tr>
                    <td>7</td>
                    <td>Vigilance /Protective Homes</td>
                    <td>5</td>
                    <td>-</td>
                    <td>5</td>
                  </tr>
                  <tr>
                    <td>8</td>
                    <td>Unmarried Mothers Home</td>
                    <td>1</td>
                    <td>1</td>
                    <td>2</td>
                </tr>
              </tbody>
            </table>
<h5><strong>V .Partners /Stakeholders</strong></h5>
<p>Government of India and Non-Governmental Organisations. There are 83 Non-Governmental Organisations who are hand in hand with the State Government run the homes and implements rehabilitation programme for the children and adults.</p>
<p><strong>CHILDREN IN NEED OF CARE AND PROTECTION</strong></p>
<p><strong>6.1. CHILD WELFARE COMMITTEES</strong></p>
<p><u>(Under section 29 (1) of the  Juvenile Justice (Care and Protection of Children) Act, 2000 as amended in 2006)</u></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="27">
<p align="center">1</p>
</td>
<td valign="top" width="217">Objective</td>
<td valign="top" width="381">To deal with neglected, abandoned, surrendered and abused children who are in need of care and protection produced before the committees and send them to their families or the Children Home</td>
</tr>
<tr>
<td valign="top" width="27">
<p align="center">2</p>
</td>
<td valign="top" width="217">Constitution</td>
<td valign="top" width="381">1 Non-official Chairperson and 4 Members (including 1 woman member)</td>
</tr>
<tr>
<td valign="top" width="27">
<p align="center">3</p>
</td>
<td valign="top" width="217">Powers</td>
<td valign="top" width="381">Powers of Metropolitan Magistrate or Judicial Magistrate of First Class as per provisions under the Code of Criminal Procedure</td>
</tr>
<tr>
<td valign="top" width="27">
<p align="center">4</p>
</td>
<td valign="top" width="217">Number of Committees</td>
<td valign="top" width="381">32 Child Welfare Committees are functioning in the State</td>
</tr>
<tr>
<td valign="top" width="27">
<p align="center">5</p>
</td>
<td valign="top" width="217">Place of Functioning</td>
<td valign="top" width="381">32 districts (Details given in Annexure-I)</td>
</tr>
<tr>
<td valign="top" width="27">
<p align="center">6.</p>
</td>
<td valign="top" width="217">Days of Meeting</td>
<td valign="top" width="381">Tuesday, Thursday and Friday every week. (From 2.00 p.m to 5.00 p.m)</td>
</tr>
<tr>
<td valign="top" width="27">
<p align="center">7</p>
</td>
<td valign="top" width="217">Venue of Meeting</td>
<td valign="top" width="381">In the premises of the notified Homes (Details given in Annexure - I)</td>
</tr>
</tbody>
</table>
<p align="center"><strong>6.2. RECEPTION UNITS</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="23">1</td>
<td valign="top" width="218">Objective</td>
<td valign="top" width="383">Providing shelter, food, clothing and education to the children under inquiry before the Child Welfare Committees</td>
</tr>
<tr>
<td valign="top" width="23">2</td>
<td valign="top" width="218">Implemented by</td>
<td valign="top" width="383">Government / Non-Governmental Organizations</td>
</tr>
<tr>
<td valign="top" width="23">3</td>
<td valign="top" width="218">Number of Units</td>
<td valign="top" width="383">23 (Government - 9, Non-Governmental Organizations -14)</td>
</tr>
<tr>
<td valign="top" width="23">4</td>
<td valign="top" width="218">Capacity of an Unit</td>
<td valign="top" width="383">30 -50 Children</td>
</tr>
<tr>
<td valign="top" width="23">5</td>
<td valign="top" width="218">Education imparted</td>
<td valign="top" width="383">Formal &amp; Non-formal Education</td>
</tr>
<tr>
<td valign="top" width="23">6</td>
<td valign="top" width="218">Extra-Curricular Activities</td>
<td valign="top" width="383">Art, Music, Theatre, Dance and Sports</td>
</tr>
<tr>
<td valign="top" width="23">7</td>
<td valign="top" width="218">Place / Jurisdiction and age limit for admission</td>
<td valign="top" width="383">Details given in Annexure - II</td>
</tr>
</tbody>
</table>
<p align="center"><strong>6.3. CHILDREN HOMES</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="23">1</td>
<td valign="top" width="218">Objective</td>
<td valign="top" width="384">Providing shelter, food and clothing to the children committed by the Child Welfare Committees for their long term rehabilitation</td>
</tr>
<tr>
<td valign="top" width="23">2</td>
<td valign="top" width="218">Implemented by</td>
<td valign="top" width="384">Government / Non-Governmental Organizations</td>
</tr>
<tr>
<td valign="top" width="23">3</td>
<td valign="top" width="218">Number of Institutions</td>
<td valign="top" width="384">30 (Government - 10, Non-Governmental Organizations - 20)</td>
</tr>
<tr>
<td valign="top" width="23">4</td>
<td valign="top" width="218">Capacity of an Unit</td>
<td valign="top" width="384">100 - 300 children</td>
</tr>
<tr>
<td valign="top" width="23">5</td>
<td valign="top" width="218">Education imparted</td>
<td valign="top" width="384">upto 8th Std (8 institutions) upto 10th Std (2 institutions)</td>
</tr>
<tr>
<td valign="top" width="23">6</td>
<td valign="top" width="218">Extra-Curricular Activities</td>
<td valign="top" width="384">Art, Music, Theatre, Dance and Sports</td>
</tr>
<tr>
<td valign="top" width="23">7</td>
<td valign="top" width="218">Place / Jurisdiction and  age group for admission</td>
<td valign="top" width="384">Details given in Annexure – III</td>
</tr>
</tbody>
</table>
<p align="center"><strong>6.4. AFTER CARE HOMES</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="23">1</td>
<td valign="top" width="218">Objective</td>
<td valign="top" width="396">Providing shelter, food and clothing to the children discharged from Children Homes</td>
</tr>
<tr>
<td valign="top" width="23">2</td>
<td valign="top" width="218">Implemented by</td>
<td valign="top" width="396">State Government</td>
</tr>
<tr>
<td valign="top" width="23">3</td>
<td valign="top" width="218">Number of Institutions</td>
<td valign="top" width="396">3  (Boys - 2, Girls - 1)</td>
</tr>
<tr>
<td valign="top" width="23">4</td>
<td valign="top" width="218">Capacity of an unit</td>
<td valign="top" width="396">50 - 100 children</td>
</tr>
<tr>
<td valign="top" width="23">5</td>
<td valign="top" width="218">Education imparted</td>
<td valign="top" width="396">Higher education in outside educational institutions, (Colleges, I.T.Is, and Polytechnics)</td>
</tr>
<tr>
<td valign="top" width="23">6</td>
<td valign="top" width="218">Extra-Curricular Activities</td>
<td valign="top" width="396">Art, Music, Theatre, Dance and Sports</td>
</tr>
<tr>
<td valign="top" width="23">7</td>
<td valign="top" width="218">Place / Jurisdiction and age limit for admission</td>
<td valign="top" width="396">Details given in Annexure –IV</td>
</tr>
</tbody>
</table>
<p><strong>CHILDREN IN CONFLICT WITH LAW</strong></p>
<p><strong>6.5. JUVENILE JUSTICE BOARDS</strong></p>
<p><u>(Under section 4 (1) of the Juvenile Justice (Care and Protection of Children) Act, 2000 as amended in 2006.)</u></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="23">1</td>
<td valign="top" width="218">Objective</td>
<td valign="top" width="396">To deal with cases of children in conflict with law.</td>
</tr>
<tr>
<td valign="top" width="23">2</td>
<td valign="top" width="218">Constitution</td>
<td valign="top" width="396">Metropolitan Magistrate or Judicial Magistrate as Chairperson and 2 Social Workers as Members (including 1 woman member)</td>
</tr>
<tr>
<td valign="top" width="23">3</td>
<td valign="top" width="218">Powers</td>
<td valign="top" width="396">Powers of Metropolitan Magistrate or a Judicial Magistrate of the first class as per the Code of Criminal Procedure.</td>
</tr>
<tr>
<td valign="top" width="23">4</td>
<td valign="top" width="218">Number of Boards</td>
<td valign="top" width="396">Juvenile Justice Boards have been constituted exclusively for every District and now such Boards are available in 32 Districts of the State.</td>
</tr>
<tr>
<td valign="top" width="23">5</td>
<td valign="top" width="218">Place of Functioning</td>
<td valign="top" width="396">32 districts (Details given in Annexure II)</td>
</tr>
<tr>
<td valign="top" width="23">6</td>
<td valign="top" width="218">Days of Meeting</td>
<td valign="top" width="396">Monday, Wednesday and Friday every week (10.00a.m to 1.00 p.m)</td>
</tr>
<tr>
<td valign="top" width="23">7</td>
<td valign="top" width="218">Venue of Meeting /Jurisdiction</td>
<td valign="top" width="396">In the premises of the notified Homes (Details given in Annexure – V)</td>
</tr>
</tbody>
</table>
<p align="center"><strong>6.6. OBSERVATION HOMES</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="23">1</td>
<td valign="top" width="194">Objective</td>
<td valign="top" width="420">Providing shelter, food and clothing to the children whose cases are pending before the Juvenile Justice Boards.</td>
</tr>
<tr>
<td valign="top" width="23">2</td>
<td valign="top" width="194">Implemented by</td>
<td valign="top" width="420">Government / Non-Governmental Organisations</td>
</tr>
<tr>
<td valign="top" width="23">3</td>
<td valign="top" width="194">Number of Institutions</td>
<td valign="top" width="420">8 (Government - 6, Non-Governmental Organisations - 2) (Details given in Annexure-VI)</td>
</tr>
<tr>
<td valign="top" width="23">4</td>
<td valign="top" width="194">Capacity of an Unit</td>
<td valign="top" width="420">50 – 100 children</td>
</tr>
<tr>
<td valign="top" width="23">5</td>
<td valign="top" width="194">Education imparted</td>
<td valign="top" width="420">Vocational Training such as cell phone servicing, two wheeler mechanism, photography, book binding and  tailoring. Formal education is also imparted to those children who are willing to study for higher standards.</td>
</tr>
<tr>
<td valign="top" width="23">6</td>
<td valign="top" width="194">Extra-Curricular Activities</td>
<td valign="top" width="420">Art, Music, Theatre, Dance and Sports</td>
</tr>
<tr>
<td valign="top" width="23">7</td>
<td valign="top" width="194">Place/Jurisdiction and age limit for admission.</td>
<td valign="top" width="420">In No Premises to the notified Homes (Details given in Annexure VI)</td>
</tr>
</tbody>
</table>
<p><strong> </strong></p>
<p align="center"><strong>6.7. SPECIAL HOMES</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="23">1</td>
<td valign="top" width="218">Objective</td>
<td valign="top" width="384">Providing shelter, food and clothing to children committed by the Juvenile Justice Boards for long term rehabilitation.</td>
</tr>
<tr>
<td valign="top" width="23">2</td>
<td valign="top" width="218">Implemented by</td>
<td valign="top" width="384">Government</td>
</tr>
<tr>
<td valign="top" width="23">3</td>
<td valign="top" width="218">Number of Institutions</td>
<td valign="top" width="384">2 (Girls 1, Boys -1) (Details given in Annexure-VII)</td>
</tr>
<tr>
<td valign="top" width="23">4</td>
<td valign="top" width="218">Capacity of an Unit</td>
<td valign="top" width="384">100 children</td>
</tr>
<tr>
<td valign="top" width="23">5</td>
<td valign="top" width="218">Education imparted</td>
<td valign="top" width="384">Formal &amp; Non-Formal Education is imparted. In addition Vocational Training such as Cell phone Servicing, Two Wheeler mechanism, Photography, Book binding and Tailoring are also imparted.</td>
</tr>
<tr>
<td valign="top" width="23">6</td>
<td valign="top" width="218">Extra-Curricular Activities</td>
<td valign="top" width="384">Art, Music, Theatre, Dance and Sports</td>
</tr>
<tr>
<td valign="top" width="23">7</td>
<td valign="top" width="218">Place/Jurisdiction and age limit for admission.</td>
<td valign="top" width="384">Details given in the Annexure VII</td>
</tr>
</tbody>
</table>
<p align="center"><strong>6.8. RESOURCE CENTRES</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="23">1</td>
<td valign="top" width="218">Objective</td>
<td valign="top" width="400">Resource Centres are to provide psycho social care and guidance to the children referred to them within that district in which it is located.</td>
</tr>
<tr>
<td valign="top" width="23">2</td>
<td valign="top" width="218">Implemented by</td>
<td valign="top" width="400">Non- Governmental Organisations</td>
</tr>
<tr>
<td valign="top" width="23">3</td>
<td valign="top" width="218">Constitution</td>
<td valign="top" width="400">1 Social Worker - cum - Counsellor and 1 Psychologist</td>
</tr>
<tr>
<td valign="top" width="23">4</td>
<td valign="top" width="218">Number of Resource Centres</td>
<td valign="top" width="400">18 (Details given in Annexure-VIII)</td>
</tr>
<tr>
<td valign="top" width="23">5</td>
<td valign="top" width="218">Other Activities</td>
<td valign="top" width="400">Counselling and guidance to the parents.   Undertake research programmes on the prevalence of juveniles in conflict with law</td>
</tr>
<tr>
<td valign="top" width="23">6</td>
<td valign="top" width="218">Place</td>
<td valign="top" width="400">Details are given in Annexure VIII</td>
</tr>
</tbody>
</table>
<p align="center"><strong>6.9. OPEN SHELTERS FOR CHILDREN IN NEED IN URBAN AND SEMI-URBAN AREAS</strong></p>
<p align="center"><strong>(SCHEME FOR CHILDREN IN NEED INCLUDING STREET CHILDREN)</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="23">1</td>
<td valign="top" width="218">Objective</td>
<td valign="top" width="384">Provide a safe place of shelter to all children in need of care and protection particularly beggars, street and working children, rag pickers, small vendors, street performers orphaned, deserted, trafficked and runaway children, children of migrant population and any other vulnerable group of children.</td>
</tr>
<tr>
<td valign="top" width="23">2</td>
<td valign="top" width="218">Implemented by</td>
<td valign="top" width="384">Non-Governmental Organisations</td>
</tr>
<tr>
<td valign="top" width="23">3</td>
<td valign="top" width="218">Number of Shelters</td>
<td valign="top" width="384">15 (Details given in Annexure-IX)</td>
</tr>
<tr>
<td valign="top" width="23">4</td>
<td valign="top" width="218">Capacity of an Unit</td>
<td valign="top" width="384">25 children (under ICPS)</td>
</tr>
<tr>
<td valign="top" width="23">5</td>
<td valign="top" width="218">Education Imparted</td>
<td valign="top" width="384">Formal &amp; Non-Formal Education and Vocational Training</td>
</tr>
<tr>
<td valign="top" width="23">6</td>
<td valign="top" width="218">Places</td>
<td valign="top" width="384">Details given in Annexure IX</td>
</tr>
</tbody>
</table>
<p><strong>6.10. CRISIS INTERVENTION CENTRE</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="23">1</td>
<td valign="top" width="218">Objective</td>
<td valign="top" width="384">To ensure prevention of child abuse and providing temporary care and protection to the abused children. To provide guidance and counselling services to the children and their families</td>
</tr>
<tr>
<td valign="top" width="23">2</td>
<td valign="top" width="218">Implemented by</td>
<td valign="top" width="384">Non-Governmental Organisation</td>
</tr>
<tr>
<td valign="top" width="23">3</td>
<td valign="top" width="218">Number of Centre</td>
<td valign="top" width="384">1</td>
</tr>
<tr>
<td valign="top" width="23">4</td>
<td valign="top" width="218">Capacity of an Unit</td>
<td valign="top" width="384">30 children</td>
</tr>
<tr>
<td valign="top" width="23">5</td>
<td valign="top" width="218">Address</td>
<td valign="top" width="384">Indian Council for Child Welfare, No.5, 3rd Main Road, Shenoy Nagar (West), Chennai. 600 030. Phone No.044-26260097/26205655</td>
</tr>
</tbody>
</table>
<p><strong> </strong></p>
<p align="center"><strong>6.11. SCHEME FOR ERADICATION OF JUVENILE BEGGARY</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="23">1</td>
<td valign="top" width="218">Objective</td>
<td valign="top" width="384">To create awareness about juvenile beggary and prevent children from begging by providing shelter, food and to rehabilitate them</td>
</tr>
<tr>
<td valign="top" width="23">2</td>
<td valign="top" width="218">Implemented by</td>
<td valign="top" width="384">Non-Governmental Organisation</td>
</tr>
<tr>
<td valign="top" width="23">3</td>
<td valign="top" width="218">Number of Shelter</td>
<td valign="top" width="384">1</td>
</tr>
<tr>
<td valign="top" width="23">4</td>
<td valign="top" width="218">Address</td>
<td valign="top" width="384">Indian Council for Child Welfare, No.5, 3rd Main Road, Shenoy Nagar (West), Chennai. 600 030. Phone No.044-26260097 / 26205655</td>
</tr>
</tbody>
</table>
<p align="center"><strong>6.12. CHILDLINE</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="23">1</td>
<td valign="top" width="218">Objective</td>
<td valign="top" width="384">To rescue and provide shelter, medical aid, repatriation, psychological support and guidance to children who are in difficult circumstances</td>
</tr>
<tr>
<td valign="top" width="23">2</td>
<td valign="top" width="218">Contact Number</td>
<td valign="top" width="384">1098</td>
</tr>
<tr>
<td valign="top" width="23">3</td>
<td valign="top" width="218">Supporting Agencies</td>
<td valign="top" width="384">BSNL, Chennai Telephones, Vodafone, Airtel, Aircel, Reliance and TATA Indicom</td>
</tr>
<tr>
<td valign="top" width="23">4</td>
<td valign="top" width="218">Places where CHILDLINE  are available at present</td>
<td valign="top" width="384">Chennai, Madurai, Thiruchirapalli, Coimbatore, Salem, Tirunelveli, Kanniyakumari, Cuddalore, Nagapattinam, Kancheepuram (Mahabalipuram), Virudhunagar, Dharmapuri, Pudukottai, Vellore, Thanjavur, Thiruvannamalai, Thiruvallur, Tirupur, Dindigul, Villupuram, Thoothukudi, Karur, Namakkal, Krishnagiri and Ramanathapuram (25 Districts).</td>
</tr>
</tbody>
</table>
<p align="center"><strong>6.13. HOMES UNDER IMMORAL TRAFFIC (PREVENTION) ACT, 1956</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="23">1</td>
<td valign="top" width="218">Objective</td>
<td valign="top" width="384">To provide care, treatment, training and rehabilitation to the women victims of trafficking, abuse and Commercial Sex Workers. To provide protection to girls who face threat of sexual exploitation and are in moral danger.</td>
</tr>
<tr>
<td valign="top" width="23">2</td>
<td valign="top" width="218">Homes run under this Act</td>
<td valign="top" width="384">
<ol start="1" type="1">
<li>Vigilance / Protective Homes</li>
<li>Rescue Shelter and</li>
<li>Unmarried / Unwed  Mothers Home</li>
<li>Stri Sadana(Voluntary Admissions)</li>
</ol>
</td>
</tr>
</tbody>
</table>
<p align="center"><strong>6.14. VIGILANCE / PROTECTIVE HOMES</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="23">1</td>
<td valign="top" width="218">Objective</td>
<td valign="top" width="384">To provide care, food, clothing, medical and health assistance and provide rehabilitation to the women and girl victims of trafficking for commercial sexual exploitation.</td>
</tr>
<tr>
<td valign="top" width="23">2</td>
<td valign="top" width="218">Implemented by</td>
<td valign="top" width="384">Government</td>
</tr>
<tr>
<td valign="top" width="23">3</td>
<td valign="top" width="218">Number of Institutions</td>
<td valign="top" width="384">5 (Protective Homes - 3, Vigilance Homes - 2) (Details given in Annexure-X )</td>
</tr>
<tr>
<td valign="top" width="23">4</td>
<td valign="top" width="218">Capacity of a Home</td>
<td valign="top" width="384">25-100</td>
</tr>
<tr>
<td valign="top" width="23">5</td>
<td valign="top" width="218">Education imparted</td>
<td valign="top" width="384">Non-Formal and Vocational Training</td>
</tr>
<tr>
<td valign="top" width="23">6</td>
<td valign="top" width="218">Financial Assistance</td>
<td valign="top" width="384">Cash assistance of Rs5000/- (or) material assistance of Rs10,000/- is given from the Tamil Nadu Social Defence Welfare Fund for Women and Children for self employment.</td>
</tr>
<tr>
<td valign="top" width="23">7</td>
<td valign="top" width="218">No of women /  girls who availed assistance so far</td>
<td valign="top" width="384">153 women</td>
</tr>
<tr>
<td valign="top" width="23">8</td>
<td valign="top" width="218">Victim Relief Fund</td>
<td valign="top" width="384">The State Government has announced a financial assistance of Rs.10,000/- to victims  rescued from trafficking for their livelihood and rehabilitation vide G.O.MS.NO.404,Home (Pol.12) Department, dated. 07.06.2012. During the year 2012-13, Rs.4,00,000/- has been disbursed to benefit 40 victims and for the year 2013-14. Rs.1,30,000/- has disbursed to benefit 13 victims so far.</td>
</tr>
<tr>
<td valign="top" width="23">9</td>
<td valign="top" width="218">Place</td>
<td valign="top" width="384">Details given in Annexure X</td>
</tr>
</tbody>
</table>
<p align="center"><strong>6.15. RESCUE SHELTERS</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="32">1</td>
<td valign="top" width="215">Objective</td>
<td valign="top" width="378">To provide immediate shelter for the rescued victims under Immoral Traffic (Prevention) Act, 1956</td>
</tr>
<tr>
<td valign="top" width="32">2</td>
<td valign="top" width="215">Implemented by</td>
<td valign="top" width="378">Government</td>
</tr>
<tr>
<td valign="top" width="32">3</td>
<td valign="top" width="215">Number of Rescue Shelter</td>
<td valign="top" width="378">5 (Attached with Vigilance / Protective Homes  (Details given in Annexure-X)</td>
</tr>
<tr>
<td valign="top" width="32">4</td>
<td valign="top" width="215">Education Imparted</td>
<td valign="top" width="378">Non-Formal  and Vocational Training</td>
</tr>
<tr>
<td valign="top" width="32">5</td>
<td valign="top" width="215">Period of Stay</td>
<td valign="top" width="378">Maximum 3 months or until the victim is restored with their family / rehabilitated as ordered by the Court.</td>
</tr>
</tbody>
</table>
<p align="center"><strong>6.16. UNMARRIED / UNWED MOTHERS HOME</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="32">1</td>
<td valign="top" width="215">Objective</td>
<td valign="top" width="378">To provide care, food, clothing medical assistance and shelter to victim women and girls who become pregnant due to sexual exploitation and abuse.</td>
</tr>
<tr>
<td valign="top" width="32">2</td>
<td valign="top" width="215">Implemented by</td>
<td valign="top" width="378">NGO</td>
</tr>
<tr>
<td valign="top" width="32">3</td>
<td valign="top" width="215">Number of Homes</td>
<td valign="top" width="378">2 (Details given in Annexure –XI)</td>
</tr>
<tr>
<td valign="top" width="32">4</td>
<td valign="top" width="215">Education Imparted</td>
<td valign="top" width="378">Non-Formal Education and Vocational Training</td>
</tr>
<tr>
<td valign="top" width="32">5</td>
<td valign="top" width="215">Place</td>
<td valign="top" width="378">Details given in Annexure XI</td>
</tr>
</tbody>
</table>
<p><strong>6.17. STRI SADANA</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="23">1</td>
<td valign="top" width="230">Objective</td>
<td valign="top" width="384">To provide care, food, clothing, medical and health assistance and provide rehabilitation to the women and girl victims of trafficking for commercial sexual exploitation.</td>
</tr>
<tr>
<td valign="top" width="23">2</td>
<td valign="top" width="218">Implemented by</td>
<td valign="top" width="384">Government</td>
</tr>
<tr>
<td valign="top" width="23">3</td>
<td valign="top" width="218">Number of Institutions</td>
<td valign="top" width="384">1(Details given in Annexure-X )</td>
</tr>
<tr>
<td valign="top" width="23">4</td>
<td valign="top" width="218">Capacity of a Home</td>
<td valign="top" width="384">25-100</td>
</tr>
<tr>
<td valign="top" width="23">5</td>
<td valign="top" width="218">Education imparted</td>
<td valign="top" width="384">Non-Formal and Vocational Training</td>
</tr>
</tbody>
</table>
<p><strong>6.18. SCHEME FOR PREVENTION OF ALCOHOLISM AND SUBSTANCE (Drugs) ABUSE</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="23">1</td>
<td valign="top" width="260">Objective</td>
<td colspan="2" valign="top" width="438">1. To create awareness and educate people about the ill-effects of alcoholism and substance abuse on the individual, family, the work place and society at large.</p>
<p>2. To provide for the whole range of community based services for identification, motivation, counselling, de-addiction, after care rehabilitation for Whole Person Recovery (WPR) of addicts.</p>
<p>3. To alleviate the consequences of drug and alcohol dependence amongst the individual, the family and society at large.</td>
</tr>
<tr>
<td valign="top" width="23">2</td>
<td valign="top" width="164">Constitution of Multipurpose Grant-in-aid  Committee</td>
<td valign="top" width="328">1.Secretary to Government,<br />
Social Welfare and Nutritious Meal<br />
Programme Department, Chennai</p>
<p>2.Secretary, Finance Department or his<br />
Representative</p>
<p>3.Secretary, Planning, Development<br />
and Special Initiative Department</p>
<p>4.Director of Social Defence, Chennai</p>
<p>5. T.T.Ranganathan Clinical and  Research<br />
Foundation, Chennai.</p>
<p>6. Terre-des-Homes Core Trust,<br />
Tiruvannamalai.</p>
<p>7. Mass Action Network India Trust,   Chennai</td>
<td valign="top" width="110">Chairperson</p>
<p>&nbsp;</p>
<p>Member</p>
<p>&nbsp;</p>
<p>Member</p>
<p>&nbsp;</p>
<p>Member<br />
Secretary</p>
<p>Member</p>
<p>&nbsp;</p>
<p>Member</p>
<p>&nbsp;</p>
<p>Member</td>
</tr>
<tr>
<td valign="top" width="23">3</td>
<td valign="top" width="164">Implemented by</td>
<td valign="top" width="328">Non-Governmental Organizations</td>
<td valign="top" width="110"></td>
</tr>
<tr>
<td valign="top" width="23">4</td>
<td valign="top" width="164">Funding sources</td>
<td valign="top" width="328">Government of India</td>
<td valign="top" width="110"></td>
</tr>
<tr>
<td valign="top" width="23">5</td>
<td valign="top" width="164">Number of implementing NGOs</td>
<td valign="top" width="328">26 (Details given in Annexure – XII)</td>
<td valign="top" width="110"></td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p align="center"><strong>6.19. SWADHAR SCHEME</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="37">1</td>
<td valign="top" width="217">Objective</td>
<td colspan="2" valign="top" width="389">To respond to the  needs of women especially those in difficult circumstances like widows, destitute, deserted women, women ex-prisoners, victims of sexual abuse and crimes including those trafficked and rescued from brothels, migrants or refugees rendered homeless due to natural calamities, women victims of terrorist violence and the like.</td>
</tr>
<tr>
<td valign="top" width="37">2</td>
<td valign="top" width="217">Implementing Organisations</td>
<td colspan="2" valign="top" width="389">Non-Governmental Organizations</td>
</tr>
<tr>
<td valign="top" width="37">3</td>
<td valign="top" width="217">Number of Centres</td>
<td colspan="2" valign="top" width="389">14 (Details given in Annexure –XIII)</td>
</tr>
<tr>
<td rowspan="5" valign="top" width="37">4</td>
<td rowspan="5" valign="top" width="217">Constitution of Empowered  Committee</td>
<td valign="top" width="293">1.Secretary to Government, Social Welfare and Nutritious Meal Programme Department, Chennai</td>
<td valign="top" width="96">Chairperson</td>
</tr>
<tr>
<td>2. Secretary, State Social Welfare Board.</td>
<td valign="top" width="96">Member</td>
</tr>
<tr>
<td>3. Chief Accounts Officer, Social Welfare and Nutritious Meal Programme Department, Chennai.</td>
<td valign="top" width="96">Member</td>
</tr>
<tr>
<td>4.Director of Social Defence,  Chennai</td>
<td valign="top" width="96">Member Secretary</td>
</tr>
<tr>
<td>5.Joint Director (Engg), Industries and Commerce Department, Chennai</td>
<td valign="top" width="96">Member</td>
</tr>
<tr>
<td valign="top" width="37">5.</td>
<td valign="top" width="217">Place</td>
<td colspan="2" valign="top" width="389">Details given in Annexure XIII</td>
</tr>
</tbody>
</table>
<p align="center"><strong>6.20.UJJAWALA SCHEME</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="23">1</td>
<td valign="top" width="218">Objective</td>
<td valign="top" width="384">
<ul type="disc">
<li>To prevent trafficking of Women and Children</li>
<li>To facilitate Rescue of victims</li>
<li>To provide Rehabilitation</li>
<li>To facilitate Re-integration</li>
<li>To Facilitate Repatriation of cross border victims.</li>
</ul>
</td>
</tr>
<tr>
<td valign="top" width="23">2</td>
<td valign="top" width="218">Implemented by</td>
<td valign="top" width="384">Non-Governmental Organizations</td>
</tr>
<tr>
<td valign="top" width="23">3</td>
<td valign="top" width="218">Number of Centres</td>
<td valign="top" width="384">8  (Details given in Annexure – XIV)</td>
</tr>
</tbody>
</table>
<p align="center"><strong>6.21. INFORMATION, COMPLAINTS AND SUGGESTIONS</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="23">1</td>
<td valign="top" width="218">Information about institutional programmes</td>
<td valign="top" width="384">Public Relation Officer / Duputy Director (Admin) O/o The Director  of Social Defence, 300, Purasawalkam High Road, Kellys, Chennai - 10</td>
</tr>
<tr>
<td valign="top" width="23">2</td>
<td valign="top" width="218">Complaints Redressal</td>
<td valign="top" width="384">Deputy Director (Administration) O/o The Director of Social Defence, 300, Purasawalkam High Road, Kellys, Chennai – 10</td>
</tr>
<tr>
<td valign="top" width="23">3</td>
<td valign="top" width="218">Appellant Authority</td>
<td valign="top" width="384">Director  of Social Defence, 300, Purasawalkam High Road, Kellys, Chennai - 600 010</td>
</tr>
<tr>
<td valign="top" width="23">4</td>
<td valign="top" width="218">Receipt of Communication</td>
<td valign="top" width="384">In person, by post, phone and e-mail</td>
</tr>
<tr>
<td colspan="3" align="left"><strong>Information Cell and Address</strong><br />
Office of the Director of Social Defence<br />
300, Purasawalkam High Road, Chennai-10<br />
Telephone No.(044) 26426421, 26427022<br />
Fax No.26612989<br />
Website; www.tnsocialdefence.com<br />
Email:     <a href="mailto:dsd.tn@nic.in">dsd.tn@nic.in</a></td>
</tr>
</tbody>
</table>
<p align="center"><strong>6.22. INTEGRATED CHILD PROTECTION SCHEME</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="23">1</td>
<td valign="top" width="218">Objective</td>
<td valign="top" width="384">To  contribute to the improvements in the well being of Children in difficult circumstances, as well as to the reduction of vulnerabilities to situations and actions that lead to abuse, neglect, exploitation, abandonment and separation of Children.</td>
</tr>
<tr>
<td valign="top" width="23">2</td>
<td valign="top" width="218">Implemented by</td>
<td valign="top" width="384">Government of India</td>
</tr>
<tr>
<td valign="top" width="23">3</td>
<td valign="top" width="218">Delivery structures under ICPS</td>
<td valign="top" width="384">1.State Project Support Unit(SPSU) 2. State Child Protection Society(SCPS) 3.District Child Protection Society(DCPS)</td>
</tr>
<tr>
<td valign="top" width="23">3(1)</td>
<td valign="top" width="218">Roles and Responsibilities of State Project Support Unit</td>
<td valign="top" width="384">
<ul type="disc">
<li>Develop a Plan of Action for initiating the implementation of ICPS.</li>
<li> Facilitate setting up of required structures and child protection mechanisms.</li>
<li>Collect, compile and regularly update the State level information on the status of Child protection institutions and key elements of their functioning in the districts.</li>
<li> Facilitate setting up and management of a state level child tracking system and a missing children website.</li>
<li> Carry out baseline survey in selected districts and follow up to assess the impact.</li>
<li>Carry out training and sensitization of the concerned officials of the line departments of the State Government/UT/Stake holders of JJA.</li>
<li>Ensure technical capacity built at centre and in selected states.</li>
<li> Develop and disseminate awareness raising materials on the ICPS.</li>
<li>Document and disseminate best practices.</li>
<li>Monitor and evaluate implementation of ICPS throughout the State/UT.</li>
</ul>
</td>
</tr>
<tr>
<td valign="top" width="23">3(2)</td>
<td valign="top" width="218">Roles and Responsibilities State Child Protection Society</td>
<td valign="top" width="384">
<ul type="disc">
<li>Contribute to the effective implementation of child protection legislation, schemes and achievement of child protection.</li>
<li>Set up, support and monitor performance of District Child Protection Societies.</li>
<li>Implementation of the Juvenile Justice System</li>
<li>Network and coordinate with all government departments</li>
<li>Network and coordinate with voluntary and civil organizations</li>
<li>Carry out need-based research and documentation activities.</li>
<li>Providing of Training and capacity building of all personnel</li>
<li>Preparation of Progress report to the Ministry of Women and Child Development, Government of India on programme implementation and fund utilization.</li>
<li>Liasioning with the Ministry of Women and Child Development, Government of India and State Child Protection Societies of other States/UTs.</li>
<li>Provide secretarial support to the State Child Protection Committee (SCPC).</li>
<li>Maintain a state level database of all children in institutional care and family based non-institutional care</li>
<li>Collection of State Level particulars of the Children who have the support of the parents and neglected.</li>
</ul>
</td>
</tr>
<tr>
<td valign="top" width="23">4</td>
<td valign="top" width="218">Roles and Responsibilities of DCPS</td>
<td valign="top" width="384">DCPS shall coordinate and implement all child rights and protection activities at district level under SCPS.</td>
</tr>
<tr>
<td valign="top" width="23">5</td>
<td valign="top" width="218">Contact Person</td>
<td valign="top" width="384">1.SPSU-Programme Manager 2.SCPS-Programme Manager 3.DCPS-DCPO’s in all 32 Districts(as per annexureXV)</td>
</tr>
<tr>
<td valign="top" width="23">6</td>
<td valign="top" width="218">Address</td>
<td valign="top" width="384">1.SPSU &amp; SCPS No.300,Purasawalkam High Road, Kellys, Chennai-600 010. Phone No.044-2642 7022 2.DCPS -(as per annexure)</td>
</tr>
</tbody>
</table>
<p><strong>6.23. STATE COMMISSION FOR PROTECTION OF CHILD RIGHTS</strong></p>
<p><strong> THE COMMISSIONS FOR PROTECTION OF CHILD RIGHTS ACT,2005 (Central Act 4 of 2006)</strong></p>
<table class="table table-bordered">
<tbody>
<tr>
<td valign="top" width="27">1</td>
<td valign="top" width="217">Objective</td>
<td valign="top" width="381">To recommend appropriate remedial measures to the Child Rights issues and making interventions in all important Child related issues like abuse and so on. To review the Protection mechanism provided under the JJ Act.</td>
</tr>
<tr>
<td valign="top" width="27">
<p align="center">2</p>
</td>
<td valign="top" width="217">Composition of the Commission</td>
<td valign="top" width="381">Chairperson and 6 Members( at least 2 shall be Women) Secretary -Director of Social Defence</td>
</tr>
<tr>
<td valign="top" width="27">
<p align="center">3</p>
</td>
<td valign="top" width="217">Activities of Commission</td>
<td valign="top" width="381">
<ul>
<li>Examine and review the safeguards provided under the Act and recommend measures for their implementation.</li>
<li>Present periodical reports on the working of those safeguards to Central Govt.</li>
<li>Inquire into violation of child rights and recommend initiation of proceedings.</li>
<li>Examine all factors that inhibit rights of children and recommend appropriate remedial measures.</li>
<li>Recommend appropriate remedial measures to children in Care and Protection including children in distress and marginalised and disadvantaged children.</li>
<li>Undertake periodical review of existing policies and make recommendations for their effective implementation in the best interest of children.</li>
<li>Undertake and promote research in the field of child rights.</li>
<li>Spread awareness to society about the safeguards available for protection of these rights through publications, the media, seminars and other available means.</li>
<li>Inspect or cause to be inspected any juvenile custodial home, or any other place of residence or institution meant for children, under the control of the Central Government or any State or any other authority, including any institution run by a social organisation, where children are detained or lodged for the purpose of treatment reformation or protection and take up with these authorities for remedial action, if found necessary.</li>
</ul>
</td>
</tr>
<tr>
<td valign="top" width="27">4</td>
<td valign="top" width="217">Activities of the Commission (contd)</td>
<td valign="top" width="381">
<ul>
<li>Inquire into complaints and take suo motu notice  of matters relating.</li>
</ul>
<ol start="1" type="I">
<li>Deprivation and violation of child rights</li>
<li>Non-implementation of laws providing for protection and development of children.</li>
</ol>
<ul>
<li>Non-compliance of policy decision, guidelines or instructions aimed at mitigating hardships t and ensuring welfare of the children and to provide relief to such children</li>
<li>Such other functions as it may consider necessary for the promotion of child rights and any other matter incidental to the above functions.</li>
<li>The Commission shall not inquire into any matter which is pending before a State rights and any other matter incidental to the above functions</li>
</ul>
</td>
</tr>
<tr>
<td valign="top" width="27">
<p align="center">5</p>
</td>
<td valign="top" width="217">Contact person &amp; Address</td>
<td valign="top" width="381">Chairperson -SCPCR</p>
<p>EVR Periyar Salai,<br />
No.183/1, Ponthamalle High Road<br />
Tailors Road, Killpakam.<br />
Chennai-600 010.<br />
Phone No.044-26421359<br />
(Details given in Annexure XVI)</td>
</tr>
</tbody>
</table>
          </p>
        </div>
      </div>
    </div>
    <div class="col-md-3">
    <?php $this->load->view('pages/sidebar'); ?>
    </div>
  </div>


</div>

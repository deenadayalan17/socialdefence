<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <section class="title">
            <h5 class="card-title pb-1 border-bottom">De-Addiction Centers</h5>
          </section>
          <section class="page-content">
            <p>The State is implementing the central scheme for the Prevention of Alcoholism and Substance (Drug) Abuse. The scheme provides for awarness generation and treatment as in-patients and out-patients. The normal treatment period is from 21 to 30 days. There are 26 centers functioning across the State through the Non-Governmental Organizations with financial support by Government of India.</p>
            <p><strong>LIST OF NON-GOVERNMENTAL ORGANISATION WORKING IN THE FIELD OF DRUG ABUSE PREVENTION PROGRAMME</strong></em></p>
            <p><strong>Integrated Rehabilitation Centre for Addicts</strong></p>
            <table class="table table-bordered" style="height: 2290px;" width="929">
            <tbody>
            <tr>
            <td valign="top" width="43">
            <p align="center">1.</p>
            </td>
            <td valign="top" width="276">
            <p>St.Pauls Educational &amp; Medical Trust,<br /> No.339/102, Arcot Road,<br /> Kodambakkam,<br /> Chennai-600 024.<br /> Phone No.044-24835375</p>
            </td>
            <td valign="top" width="43">
            <p align="center">2.</p>
            </td>
            <td valign="top" width="316">
            <p>The Voluntary Health Services, <br /> T.T.T.I. post, Taramani, <br /> Chennai – 600 113.<br /> Phone No.044-22542971 / 73<br /> 044-22541972 / 74 / 75</p>
            </td>
            </tr>
            <tr>
            <td valign="top" width="43">
            <p align="center">3</p>
            </td>
            <td valign="top" width="276">
            <p>Foundation for Infrastructure Reconstruction and Employment (FIRE),<br /> 1/82 E, Anna Nagar,<br /> Sirumugai-641 302.<br /> Coimbatore District.<br /> Phone No.04254-253021</p>
            </td>
            <td valign="top" width="43">
            <p align="center">4</p>
            </td>
            <td valign="top" width="316">
            <p>Madharnala Thondu Niruvanam, <br /> Thiruvendhipuram Main Road, <br /> Pathirikuppam Post,<br /> Cuddalore – 607 401.<br /> Phone No.04142-287239</p>
            </td>
            </tr>
            <tr>
            <td valign="top" width="43">
            <p align="center">5</p>
            </td>
            <td valign="top" width="276">
            <p align="left">Indian Institute of Women &amp; Child Health Trust, Battalagundu Road, Sempathi (P.O) Dindigul District-624 707.<br /> Phone No.0451-2556365 / 2461590</p>
            </td>
            <td valign="top" width="43">
            <p align="center">6</p>
            </td>
            <td valign="top" width="316">
            <p>Centre for Action and Rural Education (CARE),<br /> No.6, Kambar Street,<br /> Teacher’s Colony, Erode-638 011.<br /> Phone No.0424-267567</p>
            </td>
            </tr>
            <tr>
            <td valign="top" width="43">
            <p align="center">7</p>
            </td>
            <td valign="top" width="276">
            <p>CARE De-addiction cum Rehabilitation Centre, No.4/97, Anna Nagar, Thuraiyur Road, Namakkal – 637 002<br /> Cell No. 9443736367.</p>
            </td>
            <td valign="top" width="43">
            <p align="center">8</p>
            </td>
            <td valign="top" width="316">
            <p>Kalaiselvi Karunalaya Social Welfare Deepam Integrated Rehabilitation Centre for Addicts, <br /> 12, Ranganathan Nagar, Agaram Main Road,<br /> Selaiyur, Tambaram Taluk, <br /> Kancheepuram District.<br /> Phone No.044-26257779</p>
            </td>
            </tr>
            <tr>
            <td valign="top" width="43">
            <p align="center">9</p>
            </td>
            <td valign="top" width="276">
            <p>Kalaiselvi Karunalaya Social Welfare,<br /> Bodhimaram De-addition Centre, Ganesh Marriage Hall,<br /> No.2/7, North Street,<br /> Koloanoor, Villupuram.<br /> Cell No.044-26257779,<br /> 9150552525</p>
            </td>
            <td valign="top" width="43">
            <p align="center">10</p>
            </td>
            <td valign="top" width="316">
            <p>Athencottasan Muthamizh Kazhagam,<br /> 7-46 B, Mondaikad – 629 252.<br /> Kanniyakumari District.<br /> Cell No.9444691456</p>
            </td>
            </tr>
            <tr>
            <td valign="top" width="43">
            <p align="center">11</p>
            </td>
            <td valign="top" width="276">
            <p>M.S.Chellamuthu Trust &amp; Research Foundation,<br /> 643, K.K.Nagar, Madurai – 625 020.<br /> Phone No.0452-2586448,<br /> 2529234.</p>
            </td>
            <td valign="top" width="43">
            <p align="center">12</p>
            </td>
            <td valign="top" width="316">
            <p>Avvai Village Welfare Society,<br /> No.260, Public Office Road,<br /> Velippalayam,<br /> Nagapattinam District-611 001.<br /> Phone No.04365-248998</p>
            </td>
            </tr>
            <tr>
            <td valign="top" width="43">
            <p align="center">13</p>
            </td>
            <td valign="top" width="276">
            <p>Tiruchirapalli Multipurpose Social Service Society, (Keeranur Centre)<br /> Melapudur, P.B.No.12, <br /> Trichy – 620 001.<br /> Phone No. 0431-2410026 / 2467091</p>
            </td>
            <td valign="top" width="43">
            <p align="center">14</p>
            </td>
            <td valign="top" width="316">
            <p>Gandhi Peace Centre, <br /> 4/106/1, Trichy Main Road,<br /> Manjini (post), Athur Taluk, Salem -1<br /> Phone No.9150084060<br /> 9245926697</p>
            </td>
            </tr>
            <tr>
            <td valign="top" width="43">
            <p align="center">15</p>
            </td>
            <td valign="top" width="276">
            <p>Mass Welfare Association, <br /> 290A, Anna Nagar, Cheyyar – 604 407,<br /> Tiruvannamalai District.<br /> Cell No.9842058858</p>
            </td>
            <td valign="top" width="43">
            <p align="center">16</p>
            </td>
            <td valign="top" width="316">
            <p>Mass Action Network India Trust,<br /> (Manasha De-addition Centre) 14, First Floor, West Sivan Koil  Street, Vadapalani, Chennai – 600 026. Phone No.044-23650662<br /> Cell No.9444275762</p>
            </td>
            </tr>
            <tr>
            <td valign="top" width="43">
            <p align="center">17</p>
            </td>
            <td valign="top" width="276">Srivictoria Educational Society,</p>
            <p>Pudukkottai Road, Mathakkottai Village, Enathukanpati Post, Thanjavur  - 5<br /> Phone No.04362-226796</p>
            </td>
            <td valign="top" width="43">
            <p align="center">18</p>
            </td>
            <td valign="top" width="316">
            <p>Centre for Development and Communication Trust, (CENDECT), <br /> 89 A/B, 3rd West Street,<br /> Kamatchipuram (SO) – 625 520. Theni District.<br /> Cell No. 9443047245.</p>
            </td>
            </tr>
            <tr>
            <td valign="top" width="43">
            <p align="center">19</p>
            </td>
            <td valign="top" width="276">
            <p>Bharathi Women Development Centre,<br /> Kattur-Post,<br /> Manakkal Ayyampattai (via),<br /> Thiruvarur District-610 104.</p>
            <p>Phone No.04366-244377</td>
            <td valign="top" width="43">
            <p align="center">20</p>
            </td>
            <td valign="top" width="316">
            <p>Sri Ramakrishna Seva Nilayam, <br /> No.306, Kalakodi Street,<br /> Tenkasi – 627 811<br /> Tirunelveli District<br /> Cell No. 9894707352.</p>
            </td>
            </tr>
            <tr>
            <td valign="top" width="43">
            <p align="center">21</p>
            </td>
            <td valign="top" width="276">
            <p>SOC-SEAD,<br /> (Ayikudi Centre)<br /> P.B.No.395, Old Goods Shed Road, Teppakulam Post,<br /> Trichirapalli  – 620 002.<br /> Phone No. 0431-2700923</p>
            </td>
            <td valign="top" width="43">
            <p align="center">22</p>
            </td>
            <td valign="top" width="316">
            <p>Khajamalai Ladies Association, <br /> Khajamalai, <br /> Trichirapalli  – 620 023.<br /> Phone No.0431-2459655</p>
            </td>
            </tr>
            <tr>
            <td valign="top" width="43">
            <p align="center">23</p>
            </td>
            <td valign="top" width="276">
            <p>Annai Karunalaya Social Welfare Association, <br /> 25/2/7, Gingee Road, near Santhaimedu, Tindivanam -1<br /> Cell No. 9443241290,<br /> 9942979129.</p>
            </td>
            <td valign="top" width="43">
            <p align="center">24</p>
            </td>
            <td valign="top" width="316">
            <p>People’s Action Trust, Rasi veethi,<br /> Tiruvannamalai Road, <br /> Krishnagiri – 635 001.<br /> Phone No. 9282232323</p>
            </td>
            </tr>
            <tr>
            <td valign="top" width="43">
            <p align="center">25</p>
            </td>
            <td valign="top" width="276">
            <p>Society Uplift Network (SUN),<br /> Thandupathai Street,<br /> Annasagaram Post, Dharmapuri – 4</p>
            </td>
            <td valign="top" width="43">
            <p align="center">26</p>
            </td>
            <td valign="top" width="316">
            <p>Gramadhana Nirmana Sangam,<br /> Thamarakki (po),<br /> Sivagangai TK &amp; DT., Pin – 630 562<br /> Phone No. 9688344263<br /> 9442318816.</p>
            </td>
            </tr>
            </tbody>
            </table>
          </section>
        </div>
      </div>
    </div>
      <div class="col-md-3">
        <?php $this->load->view('pages/sidebar'); ?>
      </div>
    </div>
</div>

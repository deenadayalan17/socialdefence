<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <section class="title">
            <h5 class="card-title pb-1 border-bottom">Sponsership and Foster Care</h5>
          </section>
          <section class="page-content">
            <p><strong>Preamble:</strong><br>
              Sponsorship is a supplementary financial support to families to meet the educational, medical, nutritional and other needs of children with a view to improving the quality of their lives.</p>
            <p>Foster care is an arrangement whereby a child lives, usually on a temporary basis, with an extended or unrelated family member. Such an arrangement ensures that the biological parents do not lose any of their parental rights or responsibilities.</p>
            <p>The aim is to eventually reunite the child with his/her own family when the family circumstances improve and thus prevent institutionalization of children in difficult circumstances.</p>
            <p><b>Who are eligible?</b></p>
            <ul type="a">
              <li>Children in institutions who can be restored to families.</li>
              <li>Children in conditions of extreme deprivation in families with income not exceeding
              <ol>
                <li>Rs 36,000 per annum for Metro cities</li>
                <li>Rs 30,000 per annum for Other Cities</li>
                <li>Rs 24,000 per annum for Rural areas .</li>
              </ol>
              </li>
              <li>Children who are not legally free for adoption.</li>
              <li>Children whose parents are unable to care for them due to illness, death, desertion by one parent or any other crisis.</li>
            </ul>
            <p><b>How does it help?</b></p>
            <ul type="a">
              <li>Prevents child destitution</li>
              <li>Allows children to continue his/ her education and remain in his/ her family</li>
              <li>Offers child care within family setting</li>
              <li>Preserves families and encourages parents to fulfill their responsibilities</li>
              <li>Prevents institutionalization of vulnerable children</li>
            </ul>
          </section>
        </div>
      </div>
    </div>
      <div class="col-md-3">
        <?php $this->load->view('pages/sidebar'); ?>
      </div>
    </div>
</div>

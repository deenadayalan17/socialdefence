<?php $this->load->view('pages/carousel'); ?>
<div class="jumbotron text-center mb-0">
  <div class="container my-0">
    <h1>Welcome to the Department of Social Defence</h1>
    <p class="text-justify">The State of Tamil Nadu stands first in providing care for the Children and Women in difficult circumstances and protection of their rights. The Madras Children Act, 1920 was enacted for the care and protection of children by which Tamil Nadu has become the first State in India to enact an exclusive legislation for children.
       The Department of Social Defence is committed to provide caring and joyful atmosphere with appropriate rehabilitation to the girls and women in distrees and moral danger. The Department of Social Defence is implementing three important legislations in the State, namely the Juvenile Justice (Care and Protection of Children) Act, 2000, the immoral Traffic (Prevention) Act, 1956 and Protection of children from Sexual Offences Act, 2012.
      The Department of Social Defence is functioning under the Department of Social Welfare and Nutritious Meal Programme of the State Government. The Department is headed by the Director of Social Defence.
    </p>
  </div>
</div>
<div class="container">
  <div class="row mt-1">
    <div class="col-md-4">
      <div class="container">
      <div class="row text-center text-lg-left">
        <div class="col-lg-6 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-2 h-100">
            <img class="img-fluid img-thumbnail" src="<?= base_url()?>assets/images/img064.jpg" alt="">
          </a>
        </div>
        <div class="col-lg-6 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-2 h-100">
            <img class="img-fluid img-thumbnail" src="<?= base_url()?>assets/images/img058.jpg" alt="">
          </a>
        </div>
        <div class="col-lg-6 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-2 h-100">
            <img class="img-fluid img-thumbnail" src="<?= base_url()?>assets/images/img059.jpg" alt="">
          </a>
        </div>
        <div class="col-lg-6 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-2 h-100">
            <img class="img-fluid img-thumbnail" src="<?= base_url()?>assets/images/img068.jpg" alt="">
          </a>
        </div>
        <div class="col-lg-6 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-2 h-100">
            <img class="img-fluid img-thumbnail" src="<?= base_url()?>assets/images/img070.jpg" alt="">
          </a>
        </div>
        <div class="col-lg-6 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-2 h-100">
            <img class="img-fluid img-thumbnail" src="<?= base_url()?>assets/images/img065.jpg" alt="">
          </a>
        </div>
        <div class="pb-2 mx-5 px-5">
          <a href="#" class="btn btn-info justify-content-center">View Gallery</a>
        </div>
      </div>
    </div>
    </div>

    <div class="col-md-8">
      <div class="card">
        <div class="card-body">
          <nav>
            <div class="nav nav-tabs" id="home-tab" role="tablist">
              <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">What's New</a>
              <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Important Links</a>
            </div>
          </nav>
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
              <ul class="list-group my-1">
                <li class="list-group-item"><a href="<?= base_url()?>documents/notification/Accounts_officer_2018_press_release.PDF" >Recruitment of the Accounts Officer for State Child Protection Society - Press Release</a></li>
                <li class="list-group-item"><a href="<?= base_url()?>documents/notification/Accounts_Assistant.pdf" >Recruitment of the Accounts Officer for State Child Protection Society - Application</a></li>
              </ul>
            </div>
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
              <ul class="list-group my-1">
                <li class="list-group-item"><a href="http://www.wcd.nic.in" target="_blank" rel="noopener">Ministry of Women and Child Development</a></li>
                <li class="list-group-item"><a href="http://www.trackthemissingchild.gov.in" target="_blank" rel="noopener">Track the Missing Child</a></li>
                <li class="list-group-item"><a href="http://www.tn.gov.in" target="_blank" rel="noopener">Government of Tamil Nadu</a></li>
                <li class="list-group-item"><a href="http://www.childlineindia.org.in" target="_blank" rel="noopener">Child Line India Organization</a></li>
                <li class="list-group-item"><a href="http://www.mhrd.gov.in/" target="_blank" rel="noopener">Ministry of Human Resource Development</a></li>
                <li class="list-group-item"><a href="http://www.stophumantrafficking-mha.nic.in/" target="_blank" rel="noopener">Ministry of Home Affairs</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid  py-5" style="background:url('<?= base_url()?>assets/images/carousel-background.jpg');background-attachment:fixed;background-repeat:no-repeat;width:100% !important">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h1 class="text-warning">Social Defence</h1>
        <p class="text-white">All children in the society should get the rights for securing their best interest to lead a meaningful life free from fear, violence and injustice.
          The United Nation's Convention on the Rights of Children which is ratified by the Government of India prescribes the Right to survival, Right to protection, Right to development and the Right to participation as the fundamental rights of children.
          It is important to ensure protection for children from difficult circumstances by appropriate interventions and providing basic amenities such as food, clothing, shelter, education etc.,
      </div>
      <div class="col-md-6 pt-5">
        <p class="text-white"> The State of Tamil Nadu stands first in providing care for the Children and Women in difficult circumstances and protection of their rights.
          The Madras Children Act, 1920 was enacted for the care and protection of children by which Tamil Nadu has become the first State in India to enact an exclusive legislation for children.
          The Department of Social Defence is committed to provide caring and joyful atmosphere with appropriate rehabilitation to the girls and women in distrees and moral danger.
          The Department of Social Defence is implementing three important legislations in the State, namely the Juvenile Justice (Care and Protection of Children) Act, 2000, the immoral Traffic (Prevention) Act, 1956 and Protection of children from Sexual Offences Act, 2012.
        </p>
      </div>
    </div>
  </div>
</div>

<div class="flexslider carousel">
  <ul class="slides">
    <li>
      <img src="<?= base_url()?>assets/images/carousel/carousel1.jpg" />
    </li>
    <li>
      <img src="<?= base_url()?>assets/images/carousel/carousel2.jpg" />
    </li>
    <li>
      <img src="<?= base_url()?>assets/images/carousel/carousel3.jpg" />
    </li>
    <li>
      <img src="<?= base_url()?>assets/images/carousel/carousel4.jpg" />
    </li>
    <li>
      <img src="<?= base_url()?>assets/images/carousel/carousel5.jpg" />
    </li>
    <li>
      <img src="<?= base_url()?>assets/images/carousel/carousel6.jpg" />
    </li>
    <li>
      <img src="<?= base_url()?>assets/images/carousel/carousel7.jpg" />
    </li>
  </ul>
</div>

<div class="card">
  <div class="card-body pt-1 pb-0 bg-info text-white">
    <p class="text-white"><strong>Useful Documents</strong></p>
  </div>
  <div class="list-group list-group-flush">
    <a href="<?= base_url()?>/documents/citizen-charter/tamil.pdf" target="_blank"" class="list-group-item"><img src="<?= base_url()?>assets/images/pdf.jpg" > Citizen Charter Tamil</a>
    <a href="<?= base_url()?>/documents/citizen-charter/english.pdf" target="_blank"" class="list-group-item"><img src="<?= base_url()?>assets/images/pdf.jpg" > Citizen Charter English</a>
    <a href="<?= base_url()?>/documents/ujjawala-scheme.pdf" target="_blank"" class="list-group-item"><img src="<?= base_url()?>assets/images/pdf.jpg" > Ujjawala Scheme</a>
    <a href="<?= base_url()?>/documents/swadhar-scheme.pdf" target="_blank"" class="list-group-item"><img src="<?= base_url()?>assets/images/pdf.jpg" > Swadhar Scheme</a>
  </div>
</div>

<div class="card">
  <div class="card-body pt-1 pb-0 bg-info text-white">
    <p class="text-white"><strong>Forms</strong></p>
  </div>
  <div class="list-group list-group-flush">
    <a href="<?= base_url()?>/documents/forms/form1.pdf" target="_blank" class="list-group-item"><img src="<?= base_url()?>assets/images/pdf.jpg" > Form I</a>
    <a href="<?= base_url()?>/documents/forms/form2.pdf" target="_blank"" class="list-group-item"><img src="<?= base_url()?>assets/images/pdf.jpg" > Form II</a>
    <a href="<?= base_url()?>/documents/forms/form3.pdf" target="_blank"" class="list-group-item"><img src="<?= base_url()?>assets/images/pdf.jpg" > Form III</a>
    <a href="<?= base_url()?>/documents/forms/form4.pdf" target="_blank"" class="list-group-item"><img src="<?= base_url()?>assets/images/pdf.jpg" > Form IV</a>
    <a href="<?= base_url()?>/documents/forms/form5.pdf" target="_blank"" class="list-group-item"><img src="<?= base_url()?>assets/images/pdf.jpg" > Form V</a>
    <a href="<?= base_url()?>documents/downloads" class="list-group-item text-center">More...</a>
  </div>

</div>

<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title pb-1 border-bottom">Child Welfare Committee</h5>
          <p>As per Section 29(1) of the Juvenile Justice (Care and Protection of Children) Act, 2000, Child Welfare Committees have been established and functioning in all the 32 districts of Tamil Nadu.</p>
<p>                Child Welfare Committees comprises of one Chairperson and four Members of whom atleast one should be a woman. The Committees are vested with the powers of Metropolitan Magistrate or Judicial Magistrate of First Class as per the Code of Criminal Procedure, 1973. They have the mandate to conduct the proceedings of Child Welfare Committees in a child-friendly manner and not as judicial proceedings of court.</p>
<p><strong>A sum of Rs. 312.50 lakh has been provided in the Budget Estimate 2015-2016 for the effective functioning of the Child Welfare Committees.</strong>
<p> </p>
<p><strong>LIST OF CHILD WELFARE COMMITTEES </strong><br /> <strong> <em>(G.O.Ms.No.1, Social Welfare &amp; Nutritious Meal Programme Department dated 01.01.2004)</em></strong></p>
<table class="table table-bordered" style="height: 4192px;" width="935">
<tbody>
<tr class="bg-secondary text-white">
<th width="">S.No</th>
<th width="200">Place in which the committee is constituted</th>
<th width="200">Jurisdiction of the Committee</th>
<th width="350">Place of sitting of<br /> Child Welfare Committee</th>
</tr>
<tr>
<td rowspan="2" width="">
<p align="center">1.</p>
</td>
<td rowspan="2" width="">
<p align="center">Chennai</p>
</td>
<td rowspan="2" width="">Chennai District</td>
<td valign="top" width="312">1. Government Children's Home for Boys,<br /> No.58, Suryanarayana Chetty Street,<br /> Royapuram, Chennai.13.<br /> Phone No.044-25951450</td>
</tr>
<tr>
<td valign="top" width="312">2. Government Children Home for Girls,<br /> #300, Purasawalkam High Road,<br /> Kellys, Chennai - 10<br /> Phone No. 044-26421279</td>
</tr>
<tr>
<td width="">
<p align="center">2.</p>
</td>
<td width="">
<p align="center">Chengalpattu</p>
</td>
<td width="">Kancheepuram District</td>
<td valign="top" width="312">Government Special Home for Boys,<br /> G.S.T. Road,<br /> Chengalpattu - 603 002.<br /> Phone No.044 - 27424458</td>
</tr>
<tr>
<td rowspan="2" width="">
<p align="center">3.</p>
</td>
<td rowspan="2" width="">
<p align="center">Vellore</p>
</td>
<td rowspan="2" width="">Vellore</td>
<td valign="top" width="312">1.Government Reception Unit for Boys,<br /> Kakithapattarai, Sathuvachari<br /> Vellore. 632 002.<br /> Phone No.0416-2228174</td>
</tr>
<tr>
<td valign="top" width="312">2. Government After Care Organisation for women, Sunshine,<br /> # 9. Officers Line,<br /> Vellore. 632 001.<br /> Phone No.: 0416 - 2222812</td>
</tr>
<tr>
<td width="">
<p align="center">4.</p>
</td>
<td width="">
<p align="center">Villupuram</p>
</td>
<td width="">Villupuram</td>
<td valign="top" width="312">Government Children's Home for Boys,<br /> No.1,Shanmugaperuman Road,<br /> Kilperumbakkam, Villupuram-2.<br /> Phone No.04146-241702.</td>
</tr>
<tr>
<td width="">
<p align="center">5.</p>
</td>
<td width="">
<p align="center">Dharmapuri</p>
</td>
<td width="">Dharmapuri</td>
<td valign="top" width="312">Children's Home  for Boys and Girls under Vallalar Balar Illam,<br /> Nallampalli (Via), Kurinji Nagar Post,<br /> Dharmapuri District.<br /> Phone No. 04342-260582</td>
</tr>
<tr>
<td width="">
<p align="center">6.</p>
</td>
<td width="">
<p align="center">Erode</p>
</td>
<td width="">Erode</td>
<td valign="top" width="312">Government Children's  Home for Boys,<br /> SF. 151/2A, Kollukattumedu,<br /> Nanjai Oothukkuli, Lakkapuram Post,<br /> Karur Road, Erode.638 002.<br /> Phone No.0424-2401916</td>
</tr>
<tr>
<td rowspan="2" width="">
<p align="center">7.</p>
</td>
<td rowspan="2" width="">
<p align="center">Salem</p>
</td>
<td rowspan="2" width="">Salem District</td>
<td valign="top" width="312">1.  Children Home under Life Line Trust,<br /> # 55/1A, Kuruchi Colony,<br /> Gandhi Road, Salem-636 007.<br /> Ph No: 0427-2317147</td>
</tr>
<tr>
<td valign="top" width="312">2.  Children’s Home for Boys under Don Bosco Anbu Illam Social Service Society,<br /> # 230, Bretts Road,<br /> Mullavadi Gate,<br /> Salem-636 007.<br /> Ph No: 0427 - 2416631</td>
</tr>
<tr>
<td width="">
<p align="center">8.</p>
</td>
<td width="">
<p align="center">Coimbatore</p>
</td>
<td width="">Coimbatore</td>
<td valign="top" width="312">Children’s  Home  for Boys  and Girls under Don Bosco Anbu Illam,Social Service Society,<br /> No.38, GM Nagar,<br /> P.B.No.409,<br /> Ukkadam, Coimbatore.641 001.<br /> Phone No: 0422 - 2399758 / 2260758</td>
</tr>
<tr>
<td width="">
<p align="center">9.</p>
</td>
<td width="">
<p align="center">Virudhunagar</p>
</td>
<td width="">Virudhunagar District</td>
<td valign="top" width="312">Children's Home for Boys under Madurai Multipurpose Social Service Society,<br /> No.1/648, Sundaram Theater Road,<br /> Gandhi Nagar Street, Pandian Nagar,<br /> Virudhunagar-3.<br /> Ph.No.04562-2242965</td>
</tr>
<tr>
<td width="">
<p align="center">10.</p>
</td>
<td width="">
<p align="center">Dindigul</p>
</td>
<td width="">Dindigul District</td>
<td valign="top" width="312">Puduyugam Children’s Home,<br /> Karunanidhi Nagar, Palani Road,<br /> Dindigul, Dindigul District.<br /> Ph.No.04543-2424470</td>
</tr>
<tr>
<td width="">
<p align="center">11.</p>
</td>
<td width="">
<p align="center">Karaikudi</p>
</td>
<td width="">Sivaganga</td>
<td valign="top" width="312">Government Children's Home for Boys,<br /> O.Siruvayal,<br /> Karaikudi - 623 208.<br /> Phone  No. 04565-200828</td>
</tr>
<tr>
<td width="">
<p align="center">12.</p>
</td>
<td width="">Thoothukudi</td>
<td width="">Thoothukudi District</td>
<td valign="top" width="312">Children's Home under Muthu Kuviyal,<br /> No.176,Palayamkottai Road,<br /> Tuticorin-622 001.<br /> Ph.No.0461-2330313</td>
</tr>
<tr>
<td width="">
<p align="center">13.</p>
</td>
<td width="">
<p align="center">Thanjavur</p>
</td>
<td width="">Thanjavur District</td>
<td valign="top" width="312">Government Children's Home for Boys,<br /> V.O.C.Nagar,<br /> Thanjavur-613 007.<br /> Phone No. 04362-237013</td>
</tr>
<tr>
<td width="">
<p align="center">14.</p>
</td>
<td width="">
<p align="center">Tiruchirapalli</p>
</td>
<td width="">Tiruchirapalli</td>
<td valign="top" width="312">Children Home under Hope World Wide,<br /> #43, Rajiv Gandhi Nagar,<br /> Edamalaipatti pudur,<br /> Tiruchirapalli- 620 012<br /> Phone No. 0431-2473032</td>
</tr>
<tr>
<td width="">
<p align="center">15.</p>
</td>
<td width="">
<p align="center">Madurai</p>
</td>
<td width="">Madurai</td>
<td valign="top" width="312">Vidiyal Shelter Home,<br /> No.21 &amp; 22, Kennet Nagar,<br /> Muthupatti,<br /> Madurai-625 003.</td>
</tr>
<tr>
<td width="">
<p align="center">16.</p>
</td>
<td width="">
<p align="center">Tirunelveli</p>
</td>
<td width="">Tirunelveli</td>
<td valign="top" width="312">Children Home under Tirunelveli Social Service Society,<br /> P.B.No108, #2/A, St. Mark Street,<br /> Palayamkottai,<br /> Tirunelveli .<br /> Ph No: 0462-2578282</td>
</tr>
<tr>
<td width="">
<p align="center">17.</p>
</td>
<td width="">
<p align="center">Mayiladuthurai</p>
</td>
<td width="">Nagapattinam</td>
<td valign="top" width="312">Children's Home for Mentally and Physically Handicapped Children under Arivagam Thiruvavaduthurai Adheenam Thootam,<br /> Tharangambadi Salai,<br /> Mayiladuthurai, Nagapattinam.609 001.<br /> Ph.No.04363-222649</td>
</tr>
<tr>
<td width="">
<p align="center">18.</p>
</td>
<td>Tiruvallur</td>
<td width="">Tiruvallur District</td>
<td valign="top" width="312">Children Home for Boys and Girls under IRCDS,<br /> No.6, Namakkal Ramalingam Street,<br /> Rajajipuram, Tiruvallur-602 001.<br /> Phone No.04116-260084</td>
</tr>
<tr>
<td width="">
<p align="center">19</p>
</td>
<td>The Nilgiris</td>
<td valign="top" width="">The Nilgiris</td>
<td valign="top" width="312">Child Welfare Committee,<br /> No.50C, Nirmala Cottage,<br /> Old Garden Road,<br /> Udhagamandalam - 643 001.</td>
</tr>
<tr>
<td width="">
<p align="center">20</p>
</td>
<td>Theni</td>
<td valign="top" width="">Theni</td>
<td valign="top" width="312">Child Welfare Committee, Maitri Society Campus,<br /> Theni Main Road,<br /> Near Kammavar Engineering College,<br /> Koduvilarpatty,<br /> Theni Taluk and District - 625 534.</td>
</tr>
<tr>
<td width="">
<p align="center">21</p>
</td>
<td>Ariyalur</td>
<td valign="top">Ariyalur</td>
<td valign="top" width="312">Child Welfare Committee,<br /> No.15 A, Kamarajar Nagar,<br /> 2nd Street, Sendurai Road,<br /> Ariyalur District.</td>
</tr>
<tr>
<td width="">
<p align="center">22</p>
</td>
<td>Thiruvannamalai</td>
<td valign="top" width="">Thiruvannamalai</td>
<td valign="top" width="312">Child Welfare Committee, Terre  des homes<br /> Door No.102, First Floor,<br /> Block No.37, Near Perumbakkam Road,<br /> Thiruvannamalai Taluk and District.</td>
</tr>
<tr>
<td width="">
<p align="center">23</p>
</td>
<td>Krishnagiri</td>
<td valign="top" width="">Krishnagiri</td>
<td valign="top" width="312">Child Welfare Committee,<br /> 9/6, Bharathi Nagar,<br /> II Street,<br /> Krishnagiri Taluk and District.-635 001.</td>
</tr>
<tr>
<td width="">
<p align="center">24</p>
</td>
<td>Perambalur</td>
<td valign="top" width="">Perambalur</td>
<td valign="top" width="312">Child Welfare Committee,<br /> No164 M.M. Plaza  Trichy Main road,<br /> Perambalur –  1.</td>
</tr>
<tr>
<td width="">
<p align="center">25</p>
</td>
<td>Karur</td>
<td valign="top" width="">Karur</td>
<td valign="top" width="312">Child Welfare Committee,<br /> No.27/1 Ganesh Nagar,<br /> Vennamalai,<br /> Karur - 639 006.</td>
</tr>
<tr>
<td width="">
<p align="center">26</p>
</td>
<td>Tiruvarur</td>
<td valign="top" width="">Tiruvarur</td>
<td valign="top" width="312">Child Welfare Committee,<br /> No.15, South Madavilagam,<br /> M.L.A. Offices Near<br /> Tiruvarur.- 610  002</td>
</tr>
<tr>
<td width="">
<p align="center">27</p>
</td>
<td>Namakkal</td>
<td valign="top" width="">Namakkal</td>
<td valign="top" width="312">Child Welfare Committee,<br /> No.16A, Kander School  road,<br /> Namakkal  - 637 001.</td>
</tr>
<tr>
<td width="">
<p align="center">28</p>
</td>
<td>Tiruppur</td>
<td valign="top" width="">Tiruppur</td>
<td valign="top" width="312">Child Welfare Committee, Mariyalaya  Children  Home<br /> No.8/1E/122, Kasthuribai Street,<br /> Thanneer Panthal, Annanagar,<br /> Anupparpalayam Post,<br /> Tiruppur - 641 652.</td>
</tr>
<tr>
<td width="">
<p align="center">29</p>
</td>
<td>Pudukottai</td>
<td valign="top" width="">Pudukottai</td>
<td valign="top" width="312">Child Welfare Committee,<br /> Plot No/120, 1st  Floor<br /> Door no.1574 F, Sarathi Bhavanam .Bharathi  Nagar,<br /> Kattiya vayal,<br /> Rajagobalapuram post<br /> Pudukottai -622 003</td>
</tr>
<tr>
<td width="">
<p align="center">30</p>
</td>
<td>Cuddalore</td>
<td valign="top" width="">Cuddalore</td>
<td valign="top" width="312">Child Welfare Committee,<br /> Terr des homes core Trust For relief and Education<br /> 17 / 14A, Mariasusai Nagar<br /> Vannarapalayam,<br /> Cuddaore - 607  001</td>
</tr>
<tr>
<td width="">
<p align="center">31</p>
</td>
<td>Kanniyakumari</td>
<td valign="top" width="">Kanniyakumari</td>
<td valign="top" width="312">Child Welfare Committee,<br /> No.96, Narayanan Street,<br /> Christopher Nagar, NagerKoil-3<br /> Kanniyakumari -629  003</td>
</tr>
<tr>
<td width="">
<p align="center">32</p>
</td>
<td>Ramanathapuram</td>
<td valign="top" width="">Ramanathapuram</td>
<td valign="top" width="312">Child Welfare Committee,<br /> Ground Floor 105,<br /> 1/994-A, Sat Ibrahim Colony,<br /> Avvai Street,<br /> Bharathi Nagar<br /> Ramanathapuram  District  -623 503</td>
</tr>
</tbody>
</table>

        </div>
      </div>
    </div>
    <div class="col-md-3">
      <?php $this->load->view('pages/sidebar'); ?>
    </div>
  </div>


</div>

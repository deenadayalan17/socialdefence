<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title pb-1 border-bottom">Downloads</h5>
          <table class="table table-bordered alignleft">
<tbody>
<tr>
<td style="text-align: left;"><a href="<?= base_url() ?>documents/forms/form1.pdf" target="_blank" rel="noopener">Form - 1</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form2.pdf" target="_blank" rel="noopener">Form - 2</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form3.pdf" target="_blank" rel="noopener">Form - 3</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form4.pdf" target="_blank" rel="noopener">Form - 4</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form5.pdf" target="_blank" rel="noopener">Form - 5</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form6.pdf" target="_blank" rel="noopener">Form - 6</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form8.pdf" target="_blank" rel="noopener">Form - 8</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form9.pdf" target="_blank" rel="noopener">Form - 9</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form10.pdf" target="_blank" rel="noopener">Form - 10</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form11.pdf" target="_blank" rel="noopener">Form - XI (ENQUIRY REPORT OF PROBATION OFFICER TO DECLARE A CHILD AS LEGALLY FOR ADOPTION)</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form12.pdf" target="_blank" rel="noopener">Form - XII (SUPERVISION ORDER)</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form13.pdf" target="_blank" rel="noopener">Form - XIII (PROBATION CONTRACT)</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form14.pdf" target="_blank" rel="noopener">Form - XIV (PROGRESS REPORT OF PERSONS ON PROBATION SUPERVISION OR ON COMMUNITY SERVICE)</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form15.pdf" target="_blank" rel="noopener">Form - XV (PROFILE OF CHILD)</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form16.pdf" target="_blank" rel="noopener">Form - XVI (HISTORY SHEET)</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form17.pdf" target="_blank" rel="noopener">Form - XVII</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form18.pdf" target="_blank" rel="noopener">Form - XVIII (ENQUIRY REPORT OF PROBATION OFFICER TO RELEASE A CHILD ON COMMUNITY BASED PROGRAMME FROM SPECIAL HOME)</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form19.pdf" target="_blank" rel="noopener">Form - XIX (ENQUIRY REPORT OF PROBATION OFFICER ABOUT A CHILD IN CHILDREN HOME/SPECIAL HOME WHO IS TO BE DISCHARGED ON THE EXPIRY OF THE PERIOD OF PLACEMENT)</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form20.pdf" target="_blank" rel="noopener">Form - XX (REGISTER OF ADMISSION OF CHILDREN IN OBSERVATION HOME, RECEPTION UNIT IN CHILDREN HOME AND SHELTER HOME)</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form21.pdf" target="_blank" rel="noopener">Form - XXI (ADMISSION REGISTER OF CHILDREN IN SPECIAL HOME/CHILDREN HOME)</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form22.pdf" target="_blank" rel="noopener">Form - XXII (INTERVIEW REGISTER)</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form23.pdf" target="_blank" rel="noopener">Form - XXIII (PROPERTY REGISTER OF CHILDREN)</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form24.pdf" target="_blank" rel="noopener">Form - XXIV (DETAILS SHOWING THE CHILDREN DISCHARGED FOR COMMUNITY BASED Programme)</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form26.pdf" target="_blank" rel="noopener">Form - XXVI (REGISTER SHOWING THE CHILDREN IN AFTER CARE SUPERVISION)</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form27.pdf" target="_blank" rel="noopener">Form - XXVII (MONTHWISE FOLLOW-UP CHART INRESPECT OF GROWN-UP GIRLS)</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form28.pdf" target="_blank" rel="noopener">Form - XXVIII (MEDICAL HISTORY SHEET)</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form29.pdf" target="_blank" rel="noopener">Form - XXIX (CASE HISTORY FORMAT)</a></td>
</tr>
<tr>
<td><a href="<?= base_url() ?>documents/forms/form30.pdf" target="_blank" rel="noopener">Form - XXX (APPLICATION FOR CERTIFICATION)</a></td>
</tr>
</tbody>
</table>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <?php $this->load->view('pages/sidebar'); ?>
    </div>
  </div>


</div>

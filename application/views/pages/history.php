<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title pb-1 border-bottom page-title">History</h5>
          <p class="page-content">Tamil Nadu is a pioneer State in launching programmes to protect and promote the welfare of children. Children are the future of any nation, hence their well being should be of primary concern. With a view to provide services to the children, especially those in difficult circumstances, the Government of Tamil Nadu enacted a legislation way back in 1920 for the first time in the history of the country. The legislation was called as the "Tamil Nadu Children Act 1920". However, the history of reformation and rehabilitation of abandoned, neglected, destitute and delinquent children in Tamil Nadu can be traced back to the year 1887, when the first Reformatory School was started in Chengalpattu. Evolution is a phenomenon especially in the context of social life and the Government of Tamil Nadu as aforesaid was in the forefront of adapting to the social changes. The Department of Social Defence as now known was originally called the " Department of Certified Schools and Vigilance Service" with an independent Head of Department from 1947. The Juvenile Justice Act of 1986 which came into force from 2.10.87 has been replaced by Juvenile Justice (Care & Protection of Children) Act, 2000. It is the endeavour of this Department to implement this Act in letter and spirit and thus ensure care and protection to all the children of the state. Department of Social Defence also provides rehabilitation to women under Difficult circumstances and vulnerable cases under Immoral Traffic (Prevention) Act, 1956.
          </p>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <?php $this->load->view('pages/sidebar'); ?>
    </div>
  </div>
</div>

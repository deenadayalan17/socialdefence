<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <section class="title">
            <h5 class="card-title pb-1 border-bottom">Government Orders</h5>
          </section>
          <section class="page-content">
            <nav>
              <div class="nav nav-tabs" id="go-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-2016" data-toggle="tab" href="#tab-2016" role="tab" aria-controls="nav-home" aria-selected="true">2016</a>
                <a class="nav-item nav-link" id="nav-2015" data-toggle="tab" href="#tab-2015" role="tab" aria-controls="nav-profile" aria-selected="false">2015</a>
                <a class="nav-item nav-link" id="nav-2014" data-toggle="tab" href="#tab-2014" role="tab" aria-controls="nav-contact" aria-selected="false">2014</a>
                <a class="nav-item nav-link" id="nav-2013" data-toggle="tab" href="#tab-2013" role="tab" aria-controls="nav-contact" aria-selected="false">2013</a>
                <a class="nav-item nav-link" id="nav-2012" data-toggle="tab" href="#tab-2012" role="tab" aria-controls="nav-contact" aria-selected="false">2012</a>
                <a class="nav-item nav-link" id="nav-2011" data-toggle="tab" href="#tab-2011" role="tab" aria-controls="nav-contact" aria-selected="false">2011</a>
                <a class="nav-item nav-link" id="nav-2010" data-toggle="tab" href="#tab-2010" role="tab" aria-controls="nav-contact" aria-selected="false">2010</a>
              </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
              <div class="tab-pane fade show active" id="tab-2016" role="tabpanel" aria-labelledby="nav-home-tab">
                <section class="pt-3">
                  <table class="table table-bordered">
                    <tr>
                      <td><a href="<?= base_url()?>documents/govt-orders/2016/25_2016.pdf" target="_blank"><b>G.O.(Ms).No.25 Dt: February 22, 2016 </b></a><br>
                          Establishment - Department of Social Defence - Creation of 32 posts of District Child Protection Officers - Sanction - Orders issued.
                      </td>
                    </tr>
                  </table>
                </section>
              </div>

              <div class="tab-pane fade" id="tab-2015" role="tabpanel" aria-labelledby="nav-profile-tab">
                <section class="pt-3">
                  <table class="table table-bordered">
                    <tbody>
                      <tr>
                        <td><a href="<?= base_url()?>documents/govt-orders/2015/74_2015.pdf" target="_blank" rel="noopener"><b>G.O.(Ms) No. 74 Dt: November 30, 2015 </b></a><br>
                        Establishment - Transfer of the administrative control of Children Homes under the control of Director of Social Welfare and bringing into the administrative control of the Director of Social Defence - Orders - Issued.</td>
                      </tr>
                      <tr>
                        <td><a href="<?= base_url()?>documents/govt-orders/2015/59_2015.pdf" target="_blank" rel="noopener"><b>G.O.(Ms) No. 59 Dt: August 27, 2015 </b></a><br>
                        Social Welfare and Nutritious Meal Programme Department - Section 36 of the Juvenile Justice (Care and Protection of children ) Act 2000 - Social Auditing - Constitution of Social Audit Committee in the state - Orders - Issued.</td>
                      </tr>
                      <tr>
                        <td><a href="<?= base_url()?>documents/govt-orders/2015/26_2015.pdf" target="_blank" rel="noopener"><b>G.O. (Ms.) No. 26 Dt: April 17, 2015 </b></a><br>
                        Social Welfare and Nutritious Meal P rogramme Department – Pension Scheme implemented under Social Security Schemes – Ch ange of eligibility c riteria – Orders – Issued.</td>
                      </tr>
                      <tr>
                        <td><a href="<?= base_url()?>documents/govt-orders/2015/17_2015.pdf" target="_blank" rel="noopener"><b>G.O.(Ms)No.17 Dt: March 18, 2015 </b></a><br>
                        Social Defence – The Protection of Children from Sexual Offences Act, 2012- Guidelines under section 39 of the Protection of Children from Sexual Offences Act, 2012 – Orders – Issued.</td>
                      </tr>
                      <tr>
                        <td><a href="<?= base_url()?>documents/govt-orders/2015/15_2015.pdf" target="_blank" rel="noopener"><b>G.O.(Ms) No.15 Dt: March 11, 2015 </b></a><br>
                        Social Welfare and Nutritious Meal Programme Department – Tamil Nadu Juvenile Justice (Care and Protection of Children) Rules, 2001 – Re-constitution of District Advisory Committee and empower it to function as District Child Protection Committee also - Orders - Issued.</td>
                      </tr>
                      <tr>
                        <td><a href="<?= base_url()?>documents/govt-orders/2015/11_2015%20.pdf" target="_blank" rel="noopener"><b>G.O.(D) No.11 Dt: January 30, 2015 </b></a><br>
                        SWNMP Department – ICDS – Printing and Supply of WHO Growth Monitoring Registers to all AWCs, Supervisor Monitoring list Registers and OTP to ICDS functionaries under M and E component for Rs.382.74 lakh during the year 2014-2015 - Sanction – Orders – Issued.</td>
                      </tr>
                    </tbody>
                  </table>
                </section>
              </div>

              <div class="tab-pane fade" id="tab-2014" role="tabpanel" aria-labelledby="nav-contact-tab">
                <section class="pt-3">
                  <table class="table table-bordered">
                    <tr>

                    </tr>
                  </table>
                </section>
              </div>

              <div class="tab-pane fade" id="tab-2013" role="tabpanel" aria-labelledby="nav-contact-tab">
                <section class="pt-3">
                  <table class="table table-bordered">
                    <tr>
                      <td><a href="<?= base_url()?>documents/govt-orders/2013/G.O%20%28Ms%29%20No.%2010%20-%20VLCPC.pdf" target="_blank" rel="noopener"><b>G.O.(Ms).No.10 Dt: January 10, 2013 </b></a><br>
                          Social Welfare and Nutritions Meal Programme Department - Department of Social Defence - Reconstitution of Village level Watch Dog Committee so as to empower it to act also as Village Level Child Protection Committee Under Integrated Child Protection Scheme - Orders - Issued.
                      </td>
                    </tr>
                  </table>
                </section>
              </div>

              <div class="tab-pane fade" id="tab-2012" role="tabpanel" aria-labelledby="nav-contact-tab">
                <section class="pt-3">
                  <table class="table table-bordered">
                    <tbody>
                      <tr>
                        <td><a href="<?= base_url()?>documents/govt-orders/2012/G.O%20%28Ms%29%20No.%2003%20-%20DCPS.pdf" target="_blank" rel="noopener"><b>G.O.(Ms).No.03 Dt:January 09, 2012 </b></a><br />
                          Social Defence - Centrally Sponsered Scheme - Integrated Child Protection Scheme - Constitution of District Child Protection Societies in 32 districts as per provision under Section 62-A of Juvenile Justice (Care and Protection of Children) Act, 2000 as amended in 2006 - Orders - Issued.</td>
                      </tr>
                      <tr>
                      <td><a href="<?= base_url()?>documents/govt-orders/2012/G.O%20%28Ms%29%20No.%20215%20-%20BLCPC.pdf" target="_blank" rel="noopener"><b>G.O.(Ms).No.215 Dt:July 19, 2012 </b> </a><br />
                      Social Defence - Integrated Child Protection Scheme - Constituiton of Block Level Child Protection Committee under Integrated Child Protection Scheme to recommend and monitor the implementation of child protection services at block level - Orders - Issued.</td>
                      </tr>
                    </tbody>
                  </table>
                </section>
              </div>

              <div class="tab-pane fade" id="tab-2011" role="tabpanel" aria-labelledby="nav-contact-tab">
                <section class="pt-3">
                  <table class="table table-bordered">
                    <tbody>
                      <tr>
                      <td><a href="<?= base_url()?>documents/govt-orders/2011/G.O%20%28Ms%29%20No.%20123.pdf" target="_blank" rel="noopener"><b>G.O.(Ms).No.123 Dt: December 23, 2011 </b></a><br />
                      Social Defence - Integrated Child Protection Scheme - Constituiton of State Child Protection Society as per provision under Section 62 A Juvenile Justice (Care and Protection of Children) Act 2000, as amended in 2006 - Sanction of Expenditure - Orders - Issued.</td>
                      </tr>
                    </tbody>
                  </table>
                </section>
              </div>

              <div class="tab-pane fade" id="tab-2010" role="tabpanel" aria-labelledby="nav-contact-tab">
                <section class="pt-3">
                  <table class="table table-bordered">
                    <tbody>
                      <tr>
                        <td><a href="<?= base_url()?>documents/govt-orders/2010/SD_23_02_2010_24%20%281%29.pdf" target="_blank" rel="noopener"><b>G.O. (P) No.24, SWNMP Dept, dated 23.02.2010</b></a><br />
                        Grant in Aid sanctioned for the year 2009-2010 to the Salem, Idhayala Childrens Home for Girls</td>
                      </tr>
                      <tr>
                        <td><a href="<?= base_url()?>documents/govt-orders/2010/SD_11_03_2010_60.pdf" target="_blank" rel="noopener"><b>G.O. (Ms) No.60, SWNMP Dept, dated 11.03.2010</b></a><br />
                        Further Continuance for the year 2009-2010 to Tiruvannamalai Terre-Des-Homes-Core Trust Childrens Home for Girls</td>
                      </tr>
                      <tr>
                        <td><a href="<?= base_url()?>documents/govt-orders/2010/SD_26_03_2010_74.pdf" target="_blank" rel="noopener"><b>G.O. (Ms) No.74, SWNMP Dept, dated 26.03.2010</b></a><br />
                        Sanction of Expenditure for formation of JJBs in 24 Districts in the State</td>
                      </tr>
                      <tr>
                        <td><a href="<?= base_url()?>documents/govt-orders/2010/SD_31_03_2010_95.pdf" target="_blank" rel="noopener"><b>G.O. (Ms) No.95, SWNMP Dept, dated 31.03.2010</b></a><br />
                        Further Continuance for the year 2009-2010 for the Reception Units and Child Welfare Committees running under State Government and NGOs which are changed as running under NGOs on the same districts viz., Trichy, Tirunelveli, Coimbatore aned Salem Districts</td>
                      </tr>
                    </tbody>
                  </table>
                </section>
              </div>

            </div>
          </section>
        </div>
        <div class="card-footer">
          <a class="btn btn-primary text-right" href="http://www.tn.gov.in/go_view/dept/30" target="_blank">More Government Orders...</a>
        </div>
      </div>
    </div>
      <div class="col-md-3">
        <?php $this->load->view('pages/sidebar'); ?>
      </div>
  </div>
</div>

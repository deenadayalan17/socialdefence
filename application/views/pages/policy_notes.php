<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <section class="title">
            <h5 class="card-title pb-1 border-bottom">Policy Notes</h5>
          </section>
          <section class="page-content">
            <table class="table table-bordered">
              <tbody>
              <tr>
              <td><a href="<?= base_url()?>documents/policy-notes/PolicyNoteTamil.pdf" target="_blank" rel="noopener">Policy Note (<b>TAMIL</b>) </a></td>
              </tr>
              <tr>
              <td><a href="<?= base_url()?>documents/policy-notes/PolicyNoteEnglish.pdf" target="_blank" rel="noopener">Policy Note (<b>ENGLISH</b>)</a></td>
              </tr>
              </tbody>
            </table>
          </section>
        </div>
      </div>
    </div>
      <div class="col-md-3">
        <?php $this->load->view('pages/sidebar'); ?>
      </div>
    </div>
</div>

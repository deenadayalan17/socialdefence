<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <section class="title">
            <h5 class="card-title pb-1 border-bottom">Registered CCI's</h5>
          </section>
          <section class="page-content">
            <table class="table table-bordered" style="height: 219px;" width="948">
            <tbody>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/registered/Ariyalur.pdf" target="_blank" rel="noopener">Ariyalur</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Chennai.pdf" target="_blank" rel="noopener">Chennai</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Coimbatore.pdf" target="_blank" rel="noopener">Coimbatore</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/registered/Cuddalore.pdf" target="_blank" rel="noopener">Cuddalore</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Dharmapuri.pdf" target="_blank" rel="noopener">Dharmapurai</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Dindigul.pdf" target="_blank" rel="noopener">Dindigul</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/registered/Erode.pdf" target="_blank" rel="noopener">Erode</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Kancheepuram.pdf" target="_blank" rel="noopener">Kanchipuram</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Kanyakumari.pdf" target="_blank" rel="noopener">Kanyakumari</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/registered/Karur.pdf" target="_blank" rel="noopener">Karur</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Krishnagiri.pdf" target="_blank" rel="noopener">Krishnagiri</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Madurai.pdf" target="_blank" rel="noopener">Madurai</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/registered/Nagapattinam.pdf" target="_blank" rel="noopener">Nagapattinam</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Namakkal.pdf" target="_blank" rel="noopener">Namakkal</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Perambalur.pdf" target="_blank" rel="noopener">Perambalur</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/registered/Perambalur.pdf" target="_blank" rel="noopener">Pudukkottai</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Ramanathapuram.pdf" target="_blank" rel="noopener">Ramanathapuram</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Salem.pdf" target="_blank" rel="noopener">Salem</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/registered/Sivagangai.pdf" target="_blank" rel="noopener">Sivaganga</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Thanjavur.pdf" target="_blank" rel="noopener">Tanjavur</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Nilgiris.pdf" target="_blank" rel="noopener">The Nilgiris</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/registered/Theni.pdf" target="_blank" rel="noopener">Theni</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Thiruvallur.pdf" target="_blank" rel="noopener">Thiruvallur</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Thiruvarur.pdf" target="_blank" rel="noopener">Thiruvarur</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/registered/Trichy.pdf" target="_blank" rel="noopener">Tiruchirappalli</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Tirunelveli.pdf" target="_blank" rel="noopener">Tirunelveli</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Tirupur.pdf" target="_blank" rel="noopener">Tiruppur</a></td>
            </tr>
            <tr>
            <td><a href="#" rel="noopener">Tiruvannamalai</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Thoothukudi.pdf" target="_blank" rel="noopener">Thoothukkudi</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Vellore.pdf" target="_blank" rel="noopener">Vellore</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/registered/Villupuram.pdf" target="_blank" rel="noopener">Villupuram</a></td>
            <td><a href="<?= base_url() ?>documents/registration/registered/Virudhunagar.pdf" target="_blank" rel="noopener">Virudhunagar</a></td>
            <td></td>
            </tr>
            </tbody>
            </table>
          </section>
        </div>
      </div>
    </div>
      <div class="col-md-3">
        <?php $this->load->view('pages/sidebar'); ?>
      </div>
    </div>
</div>

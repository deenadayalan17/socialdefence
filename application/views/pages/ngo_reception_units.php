<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <section class="title">
            <h5 class="card-title pb-1 border-bottom">Reception Units</h5>
          </section>
          <section class="page-content">
            <table class="table table-bordered">
              <tbody>
              <tr>
              <th>S.No</th>
              <th><center>Institution to which the reception unit is attached and<br /> place of sitting of Child Welfare Committee</center><center></center></th>
              <th>Age Group</th>
              <th>Districts Covered</th>
              </tr>
              <tr>
              <td>1</td>
              <td>Children’s Home for Boys and Girls under Bala Mandir,<br /> No.8, (Old No.126) Gopathy Narayanaswamy Chetty Road,<br /> T.Nagar, Chennai - 600 017.<br /> Phone No. 044-28341921</td>
              <td>0 - 5</td>
              <td>Chennai</td>
              </tr>
              <tr>
              <td>2</td>
              <td>Children’s Home for Boys &amp; Girls under Vallalar Balar Illam,<br /> Nallampalli (Via), Kurinji Nagar Post, Dharmapuri District.<br /> Phone No. 9842048758,9486482163</td>
              <td>3 - 18</td>
              <td>Dharmapuri</td>
              </tr>
              <tr>
              <td>3</td>
              <td>Children Home for Boys &amp; Girls under IRCDS,<br /> No.6, Namakkal Ramalingam Street, Rajajipuram,<br /> Tiruvallur-1.<br /> Phone No. 044-27660084</td>
              <td>3 - 18</td>
              <td>Thiruvallur</td>
              </tr>
              <tr>
              <td>4</td>
              <td>Children's Home for Mentally and Physically Handicapped Children under Arivagam,<br /> Thiruvavaduthurai Athinam Thottam, Tharangampadi Salai,<br /> Mayiladuthurai – 609 001. Nagapatinam District.<br /> Phone No. 04363-222649</td>
              <td>3 -18</td>
              <td>Nagapattinam</td>
              </tr>
              <tr>
              <td>5</td>
              <td>Children’s Home for Boys and Girls under Puduyugam,<br /> Karunanidhi Nagar, Palani Road,<br /> Dindigul, Dindigul District.<br /> Phone No. 04543-2424970</td>
              <td>3 - 18</td>
              <td>Dindigul</td>
              </tr>
              <tr>
              <td>6</td>
              <td>Children’s Home for Boys under Madurai Multipurpose Social Service Society,<br /> No.1/648, Sundaram Theater Road, Gandhi Nagar<br /> Street, Pandian Nagar, Virudhu Nagar-3.<br /> Phone No. 04562-223881</td>
              <td>3 - 18</td>
              <td>Virudhunagar</td>
              </tr>
              <tr>
              <td>7</td>
              <td>Children' Home for Boys under Muthukuvial,<br /> Tutticorin Multi Purpose Social Service Society,<br /> No.176, Palayamkottai Road, Tuticorin-622 001.<br /> Phone No. 0461-2326878</td>
              <td>3 - 18</td>
              <td>Thoothukudi</td>
              </tr>
              <tr>
              <td>8</td>
              <td>Children Home under Tirunelveli Social Service Society,<br /> P.B.No108, #2/A, St. Mark Street,<br /> Palayamkottai, Tirunelveli .<br /> Ph No: 0462-2578282<br /> Children Home under Ho</td>
              <td>3 - 18</td>
              <td>Tirunelveli</td>
              </tr>
              <tr>
              <td>9</td>
              <td>Children Home under Hope World Wide,<br /> #43, Rajiv Gandhi Nagar,<br /> Edamalaipatti pudur, Tiruchirapalli- 620 012<br /> Phone No. 0431-2473032</td>
              <td>3 - 18</td>
              <td>Tiruchirappalli, Ariyalur and Perambalur</td>
              </tr>
              <tr>
              <td>10</td>
              <td>Children Home under Life Line Trust,<br /> #55/1A, Kuruchi Colony,<br /> Gandhi Road, Salem-636 007.<br /> Ph No: 0427-2317147</td>
              <td>3 - 18</td>
              <td rowspan="2">
              <p>Salem</td>
              </tr>
              <tr>
              <td>11</td>
              <td>Children Home for Boys under Don Bosco Anbu Illam Social Service Society,<br /> #230, Bretts Road, Mullavadi Gate, Salem.<br /> Phone No: 0427 - 2416631</td>
              <td>3 - 18</td>
              </tr>
              <tr>
              <td>12</td>
              <td>Don Bosco Anbu Illam Social Service Society,<br /> #. 38, G.M. Nagar,<br /> P.B. No, 409, Ukkadam, Coimbatore – 641 001<br /> Phone No.0422-2260758/2260778</td>
              <td>3 - 18</td>
              <td>Coimbatore &amp; Nilgiris</td>
              </tr>
              <tr>
              <td>13</td>
              <td>Vidiyal Shelter Home,<br /> # 21 &amp; 22, Kenneth Nagar,<br /> Muthupatti, Madurai East - 625 003</td>
              <td>3 - 18</td>
              <td>Madurai</td>
              </tr>
              <tr>
              <td>14</td>
              <td>Sri Arunodhayam Home for Mentally and Physically<br /> Handicapped Children,<br /> Plot No.45, Sivananda Nagar,<br /> Kolathur, Chennai-99.<br /> Phone No. 044 – 26510778 / 26511450</td>
              <td>3 - 18</td>
              <td>Nothern Districts of the State</td>
              </tr>
              </tbody>
            </table>
          </section>
        </div>
      </div>
    </div>
      <div class="col-md-3">
        <?php $this->load->view('pages/sidebar'); ?>
      </div>
    </div>
</div>

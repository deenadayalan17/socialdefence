<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <section class="title">
            <h5 class="card-title pb-1 border-bottom">State Child Protection Society</h5>
          </section>
          <section class="page-content">
          <table class="table table-bordered">
            <col width="33%"/>
            <col width="33%"/>
            <col width="33%"/>
            <tr>
                <td><a href="<?= base_url() ?>documents/contact/Chennai.pdf" target="_blank">Chennai</a></td>
                <td><a href="<?= base_url() ?>documents/contact/Coimbatore.pdf" target="_blank">Coimbatore</a></td>
                <td><a href="<?= base_url() ?>documents/contact/Cuddalore.pdf" target="_blank">Cuddalore</a></td>
            </tr>
            <tr>
                <td><a href="<?= base_url() ?>documents/contact/Dindigul.pdf" target="_blank">Dindigul</a></td>
                <td><a href="<?= base_url() ?>documents/contact/Erode.pdf" target="_blank">Erode</a></td>
                <td><a href="<?= base_url() ?>documents/contact/Kancheepuram.pdf" target="_blank">Kanchipuram</a></td>
            </tr>
            <tr>
                <td><a href="<?= base_url() ?>documents/contact/Kanyakumari.pdf" target="_blank">Kanyakumari</a></td>
                <td><a href="<?= base_url() ?>documents/contact/Karur.pdf" target="_blank">Karur</a></td>
                <td><a href="<?= base_url() ?>documents/contact/Krishnagiri.pdf" target="_blank">Krishnagiri</a></td>
            </tr>
            <tr>
                <td><a href="<?= base_url() ?>documents/contact/Madurai.pdf" target="_blank">Madurai</a></td>
                <td><a href="<?= base_url() ?>documents/contact/Nagapattinam.pdf" target="_blank">Nagapattinam</a></td>
                <td><a href="<?= base_url() ?>documents/contact/Namakkal.pdf" target="_blank">Namakkal</a></td>
            </tr>
            <tr>
                <td><a href="<?= base_url() ?>documents/contact/Nilgiris.pdf" target="_blank">Nilgiris</a></td>
                <td><a href="<?= base_url() ?>documents/contact/Perambalur.pdf" target="_blank">Perambalur</a></td>
                <td><a href="<?= base_url() ?>documents/contact/Ramanathapuram.pdf" target="_blank">Ramanathapuram</a></td>
            </tr>
            <tr>
                <td><a href="<?= base_url() ?>documents/contact/Salem.pdf" target="_blank">Salem</a></td>
                <td><a href="<?= base_url() ?>documents/contact/Sivagangai.pdf" target="_blank">Sivaganga</a></td>
                <td><a href="<?= base_url() ?>documents/contact/Thanjavur.pdf" target="_blank">Tanjavur</a></td>
            </tr>
            <tr>
                <td><a href="<?= base_url() ?>documents/contact/Theni.pdf" target="_blank">Theni</a></td>
                <td><a href="<?= base_url() ?>documents/contact/Tiruvallur.pdf" target="_blank">Thiruvallur</a></td>
                <td><a href="<?= base_url() ?>documents/contact/Tiruvarur.pdf" target="_blank">Thiruvarur</a></td>
            </tr>
            <tr>
                <td><a href="<?= base_url() ?>documents/contact/Tiruchirappalli.pdf" target="_blank">Tiruchirappalli</a></td>
                <td><a href="<?= base_url() ?>documents/contact/Tirunelveli.pdf" target="_blank">Tirunelveli</a></td>
                <td><a href="<?= base_url() ?>documents/contact/Tiruvannamalai.pdf" target="_blank">Tiruvannamalai</a></td>
            </tr>
            <tr>
                <td><a href="<?= base_url() ?>documents/contact/Thoothukkudi.pdf" target="_blank">Thoothukkudi</a></td>
                <td><a href="<?= base_url() ?>documents/contact/Vellore.pdf" target="_blank">Vellore</a></td>
                <td><a href="<?= base_url() ?>documents/contact/Villupuram.pdf" target="_blank">Villupuram</a></td>
            </tr>
            <tr>
                <td><a href="<?= base_url() ?>documents/contact/Virudhunagar.pdf" target="_blank">Virudhunagar</a></td>
                <td></td>
                <td></td>
            </tr>
            </table>
          </section>
        </div>
      </div>
    </div>
      <div class="col-md-3">
        <?php $this->load->view('pages/sidebar'); ?>
      </div>
    </div>
</div>
</div>

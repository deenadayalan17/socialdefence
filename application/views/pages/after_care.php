<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <section class="title">
            <h5 class="card-title pb-1 border-bottom">After Care</h5>
          </section>
          <section class="page-content">
            <p>The Juvenile Justice Act provides for an After-Care Program for children without family or other support &amp; services; who leave institutional care after they attain 18 years of age to sustain themselves during the transition from institutional to independent life.</p>
            <p>Who are eligible?<br>Institutionalised children who leave institutional care after they attain 18 years of age.</p>
            <p>Key Components</p>
            <ul>
              <li>Community group housing on a temporary basis for groups of 6-8 young persons.</li>
              <li>Encouragement to learn a vocation or gain employment and contribute towards the rent as well as the running of the home.</li>
              <li>Encouragement to gradually sustain themselves without state support and move out of the group home to stay in a place of their own after saving sufficient amount through their earnings</li>
              <li>Provision for a peer counsellor to stay in regular contact with these groups to discuss their rehabilitation plans and provide creative outlets for channelizing their energy and to tide over the crisis periods in their lives</li>
              <li>Providing stipend during the course of vocational training until the youth gets employment</li>
              <li>Arranging loans for youths aspiring to set up entrepreneurial activities</li>
            </ul>
            <p>How does it help?</p>
            <ul>
              <li>This programme helps the children to adapt to the society and encourages them to move away from an institution based life.</li>
              <li>It helps them to become self-sufficient and gainfully engaged in productive activity.</li>
            </ul>
          </section>
        </div>
      </div>
    </div>
      <div class="col-md-3">
        <?php $this->load->view('pages/sidebar'); ?>
      </div>
    </div>
</div>

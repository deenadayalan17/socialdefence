<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <section class="title">
            <h5 class="card-title pb-1 border-bottom">Reception Units</h5>
          </section>
          <section class="page-content">
            <table class="table table-bordered" style="height: 904px;" width="936">
              <tbody>
              <tr>
                <th>S.No</th>
                <th><center>Institution to which the reception unit is attached and<br /> place of sitting of Child Welfare Committee</center><center></center></th>
                <th>Age Group</th>
                <th>Districts Covered</th>
              </tr>
              <tr>
                <td>1</td>
                <td>Government Children’s Home for Boys,<br /> No.58, Suryanarayana Chetty Street, Royapuram,<br /> Chennai-600 013.<br /> Phone No. 044-25951450</td>
                <td>5 - 18</td>
                <td rowspan="2">Chennai</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Government Children’s Home for Girls,<br /> No.300, Purasawalkam High Road, Kellys,<br /> Chennai - 600 010.<br /> Phone No. 044- 26421279</td>
                <td>5 - 18</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Government Children’s Home for Boys,<br /> S.F.151/2A, Kollukattu Medu, Nanjai Uthukulli,<br /> Lakapuram Post, Karur Road, Erode - 638 002.<br /> Phone No. 0424-2401916</td>
                <td>3 - 18</td>
                <td>Erode</td>
              </tr>
              <tr>
                <td>4</td>
                <td>Government Children’s Home for Boys,<br /> No.1, Shanmuga Peruman Road, Kilperumbakkam,<br /> Villupuram – 605 602.<br /> Phone No. 04146-241702</td>
                <td>3 -18</td>
                <td>Villupuram</td>
              </tr>
              <tr>
                <td>5</td>
                <td>Government Reception Unit for Boys,<br /> Kakithapattarai, Sathuvachari<br /> Vellore. 632 002<br /> Phone No.0416-2228174</td>
                <td>3 - 18</td>
                <td rowspan="2">Vellore</td>
              </tr>
              <tr>
                <td>6</td>
                <td>Government Aftercare Home for Women<br /> #9, Officers Line,<br /> Sunshine, Vellore – 632 001<br /> Phone No.0416-2222812</td>
                <td>3 - 18</td>
              </tr>
              <tr>
                <td>7</td>
                <td>Government Special Home for Boys,<br /> G.S.T. Road, Chengalpattu – 603 002.<br /> Phone No.044 – 27424458</td>
                <td>3 - 18</td>
                <td>Kancheepuram</td>
              </tr>
              <tr>
                <td>8</td>
                <td>Government Children’s Home for Boys,<br /> V.O.C. Nagar, Thanjavur.613007.<br /> Phone No. 04362-237013</td>
                <td>3 - 18</td>
                <td>Thanjavur</td>
              </tr>
              <tr>
                <td>9</td>
                <td>Government Children’s Home for Boys,<br /> “O” Siruvayal, Karaikudi – 623 208.<br /> Phone No. 04565–200828</td>
                <td>3 - 18</td>
                <td>Sivagangai</td>
              </tr>
              </tbody>
            </table>
          </section>
        </div>
      </div>
    </div>
      <div class="col-md-3">
        <?php $this->load->view('pages/sidebar'); ?>
      </div>
    </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <section class="title">
            <h5 class="card-title pb-1 border-bottom">Acts and Rules</h5>
          </section>
          <section class="page-content">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <td><a href="<?= base_url() ?>documents/acts-rules/TamilNadu-Juvenile-Justice-Draft-Rules-2016.pdf" target="_blank" rel="noopener">Tamil Nadu Juvenile Justice Draft Rules, 2016</a></td>
                </tr>
                <tr>
                  <td><a href="<?= base_url() ?>documents/acts-rules/JJAct2015.pdf" target="_blank" rel="noopener">Juvenile Justice Act 2015</a></td>
                </tr>
                <tr>
                  <td><a href="<?= base_url() ?>documents/acts-rules/JJact2001.pdf" target="_blank" rel="noopener">The Tamil Nadu Juvenile Justice (Care and Protection of Children) Rules, 2001</a></td>
                </tr>
                <tr>
                  <td><a href="<?= base_url() ?>documents/acts-rules/ImmoraltrafficAct1956.pdf" target="_blank" rel="noopener">The Immoral Traffic (Prevention) Act, 1956</a></td>
                </tr>
                <tr>
                  <td><a href="<?= base_url() ?>documents/acts-rules/pocso_act_2012.pdf" target="_blank" rel="noopener">Protection of Children from Sexual Offences Act, 2012.</a></td>
                </tr>
                <tr>
                  <td><a href="<?= base_url() ?>documents/acts-rules/pocso-model-guidelines.pdf" target="_blank" rel="noopener">Model Guidelines – POCSO Act</a></td>
                </tr>
              </tbody>
          </table>
        </section>
        </div>
      </div>
    </div>
      <div class="col-md-3">
        <?php $this->load->view('pages/sidebar'); ?>
      </div>
    </div>
</div>
</div>

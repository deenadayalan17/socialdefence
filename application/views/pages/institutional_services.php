<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <section class="title">
            <h5 class="card-title pb-1 border-bottom">Institutional Services</h5>
          </section>
          <section class="page-content">
          <p><strong>Preamble:</strong><br><br>
                As provided by the Juvenile Justice (Care and Protection of Children) Act 2015, the scheme shall support the creation of new institutional facilities and maintenance of existing institutional facilities for both children in conflict with law and children in need of care and protection.
                These include Shelter Homes, Children’s Homes and Observation Homes, Special Homes, Place of Safety.
          </p>
          <p>In addition, the scheme shall also provide for institutional care of children with special needs by supporting a specialized unit within the existing homes or by setting up a specialized Shelter Home for children with special needs.
            The statutory duties and responsibilities of the personnel will be as per the provisions of the Central Model Rules/State Rules under Juvenile Justice Act, 2015.
          </p>
          </section>
        </div>
      </div>
    </div>
      <div class="col-md-3">
        <?php $this->load->view('pages/sidebar'); ?>
      </div>
    </div>
</div>

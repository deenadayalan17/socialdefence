<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <section class="title">
            <h5 class="card-title pb-1 border-bottom">Special Homes</h5>
          </section>
          <section class="page-content">
            <p>If a juvenile in conflict with law is found guilty by the Juvenile Justice Board and committed by the Board, they are sent to the Special Homes for a long term rehabilitation process for a period not exceeding three years. There are two Government Special Homes, one at Chennai for girls and another at Chengalpattu for Boys. In Special Homes, the children are provided with education, vocational training, counseling and facilities for co-curricular activities to mould them as a law abiding and responsible citizen so as to enable them to mingle with the main stream of the society. To ensure the of the juvenilies, the Government have installed surveilance and security equipments at Government Special Home in Chengalpattu with 100 KVA Automatic Diesel Genset for uninterrupted power supply.</p>
            <p>The parents are provided with Free Travel Concession quarterly to see their children in child care institutions.</p>
            <table class="table table-bordered">
              <tbody>
                <tr class="bg-secondary text-white">
                  <th>S.No</th>
                  <th>Name of the Institution</th>
                  <th>Age group for admission</th>
                  <th>Proposed Sanctioned Strength</th>
                  <th>Districts covered</th>
                </tr>
                <tr>
                  <td>1.</td>
                  <td>Government Special Home for Boys,<br /> G.S.T. Road,<br /> Chengalpattu – 603 002. <br /> Phone No.044 – 27424458</td>
                  <td>8-18</td>
                  <td>100</td>
                  <td>Whole State</td>
                </tr>
                <tr>
                  <td>2.</td>
                  <td>Government Special Home for Girls,<br /> No.300, Purasawalkam High Road, Kellys, <br /> Chennai-600 010.<br /> Phone No.044-2642 1279</td>
                  <td>8-18</td>
                  <td>100</td>
                  <td>Whole State</td>
                </tr>
              </tbody>
            </table>
          </section>
        </div>
      </div>
    </div>
      <div class="col-md-3">
        <?php $this->load->view('pages/sidebar'); ?>
      </div>
    </div>
</div>

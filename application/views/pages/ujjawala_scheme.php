<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <h3 class="card-title page-title pb-1 border-bottom">Ujjawala Scheme</h3>
          <p>Ujjawala scheme has been introduced by the Government of India, to prevent trafficking of women and children for commercial sexual exploitation, which is a crime and violation of Human Rights. It is necessary for taking preliminary action to prevent trafficking in areas where probability of Trafficking is higher. Rehabilitation and Reintegration are equally important while dealing with Commercial Sexual Exploitation.</p>
          <p>This Scheme is a comprehensive scheme for Prevention of Trafficking, Rescue, Rehabilitation and Re-integration of victims of trafficking for Commercial Sexual Exploitation.</p>
          <p>There are 8 Non-Governmental Organisations implementing this scheme in the State of Tamil Nadu with financial assistance from Government of India.</p>
          <h5><strong>List of NGOs implementing UJJAWALA Scheme</strong></h5>
          <table class="table table-bordered">
            <tbody>
              <tr class="bg-secondary text-white">
                <th>S.No</th>
                <th>Name and Address of NGO</th>
                <th>Programme</th>
              </tr>
            <tr>
              <td>1.</td>
              <td>Madras Christian Council of Social Service, <br /> 21, 6th Main Road, Jawahar Nagar, Chennai - 82<br /> Phone No.044-26700744,26705486.</td>
              <td>Prevention, Rescue, Rehabilitation &amp; Re-integration</td>
            </tr>
            <tr>
              <td>2.</td>
              <td>Society for Education Research Village Empowerment, (SERVE)<br /> Sirupakkam (p.o) – 606 123<br /> Thittakudi Taluk, <br /> Cuddalore District.<br /> Cell No. 9443828169</td>
              <td>Prevention</td>
            </tr>
            <tr>
              <td>3.</td>
              <td>Rural Education for Community organization(RECO),<br /> 12, Chola Real Estate,<br /> Dharun complex, Thirugokarnam,<br /> Pudukkottai -622 002<br /> Cell No. 9600228482, 9750784719</td>
              <td>Prevention, Rescue, Rehabilitation &amp; Re-integration</td>
            </tr>
            <tr>
            <td>4</td>
              <td>Women Action Group,<br /> Gangavalli (p.o) &amp; Taluk,<br /> Salem – 636 105<br /> Phone No. 04282-232219<br /> Cell No. 9442770986</td>
              <td>Prevention</td>
              </tr>
            <tr>
              <td>5.</td>
              <td>Bharatha Matha Family Welfare Association,<br /> #23, B, Mannai Road, Thiruthuraipoondi, <br /> Tiruvarur District<br /> Phone No. 04369-295185<br /> Cell No. 9942227001, 9942227002</td>
              <td>Prevention</td>
              </tr>
            <tr>
              <td>6.</td>
              <td>Society for Rural Development Promotion Services (SRDPS),<br /> 437/1, Pasumai Nagar,<br /> Pachal post – 635 601<br /> Tirupattur Taluk, Vellore Dt.<br /> Cell No.9486962515, 9443437647</td>
              <td>Prevention, Rescue, Rehabilitation &amp; Re-integration</td>
            </tr>
            <tr>
              <td>7.</td>
              <td>Annai Karunalaya Social Welfare Association,<br /> No 25/2/7, Gingee Road,<br /> Near Sandhaimedu,<br /> Tindivanam- 604001<br /> Phone No. 04147 - 228565<br /> Cell No. 9443241290.</td>
              <td>Prevention</td>
            </tr>
            <tr>
              <td>8.</td>
              <td>Rural Education for Action Development Agency,<br /> H-27, 5th Cross, 2nd Main Road,<br /> R.M. Colony, Dindigul - 624 001.<br /> Phone No. 0451-2460838</td>
              <td>Prevention, Rescue, Rehabilitation &amp; Re-integration</td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <?php $this->load->view('pages/sidebar'); ?>
    </div>
  </div>


</div>

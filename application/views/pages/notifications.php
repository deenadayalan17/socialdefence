<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title pb-1 border-bottom">Notifications</h5>
          <table class="table table-bordered">
            <tbody>
              <tr>
                <td><a href="<?= base_url() ?>documents/notification/Important%20Circular%20to%20inmates%20and%20ex-inmates.PDF" target="_blank" rel="noopener">Important Circular to inmates and ex-inmates</a></td>
              </tr>
              <tr>
                <td><a href="<?= base_url() ?>documents/notification/call-letter_eligible-list.pdf" target="_blank" rel="noopener"> C &amp; D Recruitment - Interview call letter for eligible candidates</a></td>
              </tr>
              <tr>
                <td><a href="<?= base_url() ?>documents/notification/Circular%20-%20Comments%20and%20Suggestions%20for%20TN%20Rules%202016%20for%20JJ%20Act%202015.pdf" target="_blank" rel="noopener">Requesting for Comments and Suggestions on the TN draft rules 2016 for the JJ Act 201</a></td>
              </tr>
              <tr>
                <td><a href="<?= base_url() ?>documents/notification/Inmates%20and%20Ex-inmates%20recruitment%20-%20Press%20release.pdf" target="_black">Inmates and Ex-inmates recruitment press release</a></td>
              </tr>
              <tr>
                <td><a href="<?= base_url() ?>documents/notification/Press%20release%20DCPO%20Recuritment%20-%20Contractual%20basis.pdf" target="_blank" rel="noopener">Recruitment of District Child Protection Officers on Contractual Basis - Press Release</a></td>
              </tr>
              <tr>
                <td><a href="<?= base_url() ?>documents/notification/Application%20Format.pdf" target="_blank" rel="noopener">Recruitment of District Child Protection Officers on Contractual Basis - Application Format</a></td>
              </tr>
              <tr>
                <td><a href="<?= base_url() ?>documents/notification/Foster%20Care%20advertisement.pdf" target="_blank" rel="noopener">Advertisements - Foster Care</a></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <?php $this->load->view('pages/sidebar'); ?>
    </div>
  </div>
</div>

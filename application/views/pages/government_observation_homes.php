<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <section class="title">
            <h5 class="card-title pb-1 border-bottom">Observation Homes</h5>
          </section>
          <section class="page-content">
            <p>Observation Homes are meant for the temporary reception of those Juvenile in conflict with law who are not realeased on bail and their cases are pending before the Juvenile Justice Boards. At present in Tamil Nadu, there are six Observarion Homes directly run by Government. The children residing in the Observation Homes are provided with basic amenities like food, clothing, shelter and bedding apart from other services like education (both formal and non-formal), vocational training, medical facility and counselling as the part of their short term rehabilitation. To ensure the safety of the children, the Government have installed surveillance and security equipments at Observation Homes in Chennai and Cuddalore.</p>
            <table class="table table-bordered" style="height: 1065px;" width="928">
              <tbody>
                <tr class="bg-secondary text-white">
                  <th> S. No</th>
                  <th>Name of the Institution</th>
                  <th>Age group for admission</th>
                  <th>Proposed Sanctioned Strength</th>
                  <th>Districts covered</th>
                </tr>
                <tr>
                  <td>1.</td>
                  <td>Government Observation Home for Boys and Girls,<br> 300, Purasawalkam High Road,  <br> Chennai – 600 010.<br> Phone No. 044- 2642 2644</td>
                  <td>8-18</td>
                  <td>100</td>
                  <td>Chennai, Kancheepuram, Vellore and Tiruvallur</td>
                </tr>
                <tr>
                  <td>2.</td>
                  <td>Government Observation Home  for Boys and Girls,<br> N.G.O.'B' Colony Bus Stop,<br> Melapalayam P.O., <br> Tirunelveli – 627 003.<br>
                  Phone No. 0462-2352954</td>
                  <td>8-18</td>
                  <td>50</td>
                  <td>Tirunelveli, Thoothukudi  and Kanyakumari</td>
                </tr>
                <tr>
                  <td>3.</td>
                  <td>Government Observation Home for Boys attached to Government Children’s Home, V.O.C.Nagar, <br> Thanjavur - 613 007.<br>
                  Phone No. 04362-237013</td>
                  <td>8-18</td>
                  <td>50</td>
                  <td>Thanjavur, Tiruvarur and Nagapattinam</td>
                </tr>
                <tr>
                  <td>4.</td>
                  <td>Government Observation Home for Boys and Girls,<br> No.34, East Bouleward Road,<br> Tiruchirapalli – 620 002.<br>
                  Phone No.0431-2702234</td>
                  <td>8-18</td>
                  <td>50</td>
                  <td>Tiruchirapalli, Perambalur, Karur, Thanjavur, Pudukkottai, Tiruvarur and Nagapattinam</p>
                  </td>
                </tr>
                <tr>
                  <td>5.</td>
                  <td>Government Observation Home for  Girls, District Court Compound, Yercaud Road, Hasthampatti,<br> Salem – 636 007. <br> Phone No.0427-2415148
                  </td>
                  <td>8-18</td>
                  <td>50</td>
                  <td>Salem, Dharmapuri and Namakkal</td>
                </tr>
                <tr>
                  <td>6.</td>
                  <td>Government Observation Home for Boys and Girls, <br> Annamalai Nagar,<br> Gondur, Cuddalore-607 002.  <br> Phone No. 04142-203050</td>
                  <td>8-18</td>
                  <td>50</td>
                  <td>Thiruvanamalai, Cuddalore and Villupuram</td>
                </tr>
              </tbody>
            </table>
          </section>
        </div>
      </div>
    </div>
      <div class="col-md-3">
            <?php $this->load->view('pages/sidebar'); ?>
      </div>
  </div>
</div>

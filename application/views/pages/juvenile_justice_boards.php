<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title pb-1 border-bottom">Juvenile Justice Boards</h5>
          <p>As per section 4 of the Juvenile Justice (Care and Protection of Children) Act, 2000, Juvenile Justice Boards have been constituted in all the 32 Districts of the State. These Boards are dealing with the cases relating to the Juvenile in Conflict Law.</p>
          <p>Juvenile Justice Board consists of a Metropolitan magistrate or a Judicial Magistrate of the First Class with two Social Worker Members of whom one should be a women.</p>
          <p>These Boards hold sittings three days in a week on Monday, Wednesday and Friday in the premises of the respective Observation Homes / Notified places.</p>
          <p><b>A sum of Rs. 285.10 lakh has been provided in the Budget Estimate 2015-2016 for the effective running of Juvenile Justice Boards.</b></p>
          <p><b>LIST OF JUVENILE JUSTICE BOARDS</b> <br /> (G.O.Ms.No.151, Social Welfare &amp; Nutritious Meal Programme Department dated 22.10.2002)</p>
          <table class="table table-bordered">
            <tbody>
              <tr class="bg-secondary text-white">
                <th>S No.</th>
                <th>Place in which the Board is constituted</th>
                <th>Jurisdiction of the Board</th>
                <th>Place of sitting of Juvenile Justice Board</th>
              </tr>
              <tr>
                <td>1.</td>
                <td>Chennai</td>
                <td>Chennai</td>
                <td>Government Observation Home for Boys and Girls,<br /> No.300, Purasawalkam High Road, Kellys,  Chennai – 600 010. <br /> Phone No. 044-26422644</p></td>
              </tr>
              <tr>
                <td>2.</td>
                <td>Tirunelveli</td>
                <td>Tirunelveli</td>
                <td>Government Observation Home  for Boys and Girls,<br /> N.G.O.'B' Colony Bus Stop,<br /> Melapalayam P.O. <br /> Tirunelveli-627 003.<br /> Phone No.0462-2352954</p></td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>3.</p>
              </td>
              <td valign="top" width="144">
              <p>Thanjavur</p>
              </td>
              <td valign="top" width="192">
              <p>Thanjavur</p>
              </td>
              <td valign="top" width="288">
              <p>Government Observation Home for Boys,<br /> Government Children's Home premises,<br /> V.O.C.Nagar, Thanjavur-613 007.<br /> Phone No.04362-237013</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>4.</p>
              </td>
              <td valign="top" width="144">
              <p>Salem</p>
              </td>
              <td valign="top" width="192">
              <p>Salem</p>
              </td>
              <td valign="top" width="288">
              <p>Government Observation Home for Boy and Girls,<br /> District Court  Compound, <br /> Yercaud Road, Hasthampatti,<br /> Salem – 636 007. <br /> Phone No.0427-2415148</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>5.</p>
              </td>
              <td valign="top" width="144">
              <p>Cuddalore</p>
              </td>
              <td valign="top" width="192">
              <p>Cuddalore</p>
              </td>
              <td valign="top" width="288">
              <p>Government Observation Home for Boys and Girls, <br /> Annamalai Nagar,<br /> Gondur, Cuddalore-607 002.  <br /> Phone No. 04142-203050</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>6.</p>
              </td>
              <td valign="top" width="144">
              <p>Tiruchirapalli</p>
              </td>
              <td valign="top" width="192">
              <p>Tiruchirapalli</p>
              </td>
              <td valign="top" width="288">
              <p>Government Observation Home   for Boys and Girls,<br /> No.34, East Boulevard Road, <br /> Trichy – 620 002.<br /> Phone No.0431-2702234</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>7.</p>
              </td>
              <td valign="top" width="144">
              <p>Madurai</p>
              </td>
              <td valign="top" width="192">
              <p>Madurai</p>
              </td>
              <td valign="top" width="288">
              <p>Observation Home  for Boys and Girls under  Madurai Children  Aid  Society, <br /> No.164, Ramnad Road, <br /> (Kamarajar Road), <br /> Madurai – 625 009. <br /> Phone No. 0452-2626524</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>8.</p>
              </td>
              <td valign="top" width="144">
              <p>Coimbatore</p>
              </td>
              <td valign="top" width="192">
              <p>Coimbatore</p>
              </td>
              <td valign="top" width="288">
              <p>Observation Home for Boys and Girls Under Discharged Prisoner's<br /> Aid Society,<br /> No.1093,1094,Avinashi Road,<br /> Pappanaickenpalayam, <br /> Coimbatore – 641 037<br /> Phone No.0422-2216183</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>9</p>
              </td>
              <td valign="top" width="144">
              <p>Virudhunagar</p>
              </td>
              <td valign="top" width="192">
              <p>Virudhunagar</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board,<br /> No.2/818 Sulakkaraimedu, <br /> Virudhunagar – 626 003.</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>10</p>
              </td>
              <td valign="top" width="144">
              <p>Karur</p>
              </td>
              <td valign="top" width="192">
              <p>Karur</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board,<br /> No.248, Claret Nagar,<br /> Vennamalai, <br /> Karur – 639 006.</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>11</p>
              </td>
              <td valign="top" width="144">
              <p>Thiruvarur.</p>
              </td>
              <td valign="top" width="192">
              <p>Thiruvarur.</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board,<br /> Plot No.17, Guruthatchinamoorthy Nagar, <br /> III Cross  Street,<br /> Thanjai Salai,<br /> Thiruvarur.-6100  082</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>12</p>
              </td>
              <td valign="top" width="144">
              <p>Dindigul</p>
              </td>
              <td valign="top" width="192">
              <p>Dindigul</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board,<br /> No.3/327, Sundarapuri,<br /> Nallamanar Kottai Post,<br /> Dindigul – 624 005.</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>13</p>
              </td>
              <td valign="top" width="144">
              <p>Theni</p>
              </td>
              <td valign="top" width="192">
              <p>Theni</p>
              </td>
              <td width="288">
              <p>Juvenile Justice Board,<br /> No.9, Sidco  Valakam,<br /> Madurai road.<br /> Theni District.</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>14</p>
              </td>
              <td valign="top" width="144">
              <p>Erode</p>
              </td>
              <td valign="top" width="192">
              <p>Erode</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board,<br /> No.14/1, Eraniyan Street,<br /> Solar, Erode – 638 002.</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>15</p>
              </td>
              <td valign="top" width="144">
              <p>Nagapattinam</p>
              </td>
              <td valign="top" width="192">
              <p>Nagapattinam</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board,<br /> G.R.K Colony, <br /> Perumal South Street,<br /> Nagapattinam Taluk and District.</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>16</p>
              </td>
              <td valign="top" width="144">
              <p>Thiruvannamalai</p>
              </td>
              <td valign="top" width="192">
              <p>Thiruvannamalai</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board,<br /> EIC  Building,<br /> Perumbakkam,<br /> Next  to Rangammal Hospital,<br /> Thiruvannamalai Taluk and District.</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>17</p>
              </td>
              <td valign="top" width="144">
              <p>Krishnagiri</p>
              </td>
              <td valign="top" width="192">
              <p>Krishnagiri</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board,<br /> 9/6, Bharathi Nagar,  II Street,<br /> Krishnagiri Taluk and District.-635 001.</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>18</p>
              </td>
              <td valign="top" width="144">
              <p>The Nilgiris</p>
              </td>
              <td valign="top" width="192">
              <p>The Nilgiris</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board,<br /> No.50C, Nirmala Cottage,<br /> Old Garden Road,<br /> Udhagamandalam – 643 001.</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>19</p>
              </td>
              <td valign="top" width="144">
              <p>Namakkal</p>
              </td>
              <td valign="top" width="192">
              <p>Namakkal</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board,<br /> No.16A, Kander School  road,<br /> Namakkal  - 637 001.</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>20</p>
              </td>
              <td valign="top" width="144">
              <p>Tiruppur</p>
              </td>
              <td valign="top" width="192">
              <p>Tiruppur</p>
              </td>
              <td width="288">
              <p>Juvenile Justice Board,<br /> No.1/796, Appavu Nagar,<br /> Pavazhanji Palayam,<br /> Tharapuram  Road Tiruppur – 8.</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>21</p>
              </td>
              <td valign="top" width="144">
              <p>Kancheepuram</p>
              </td>
              <td valign="top" width="192">
              <p>Kancheepuram</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board,<br /> Government Special Home Campus,<br /> GST Road,<br /> Chengalpat. Kancheepuram. District.</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>22</p>
              </td>
              <td valign="top" width="144">
              <p>Vellore</p>
              </td>
              <td valign="top" width="192">
              <p>Vellore</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board,<br /> Government Garment and Hold All Making and Training Unit Campus,<br /> Sun Shine, Officers Lane,<br /> Vellore – 632 001.</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>23</p>
              </td>
              <td valign="top" width="144">
              <p> Pudukottai</p>
              </td>
              <td valign="top" width="192">
              <p>Pudukottai</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board,<br /> Plot No.4,1st Floor<br /> Door No.3674, Sakthi Nagar ,<br /> Alangudi road ,<br /> Pudukottai District-622  001          </p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>24</p>
              </td>
              <td valign="top" width="144">
              <p>Dharmapuri</p>
              </td>
              <td valign="top" width="192">
              <p>Dharmapuri</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board,<br /> Vallalar Children Home, <br /> kurinji Nagar,<br /> Thoppur via,<br /> Dharmapuri  District – 636 705</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>25</p>
              </td>
              <td valign="top" width="144">
              <p>Sivagangai</p>
              </td>
              <td valign="top" width="192">
              <p>Sivagangai</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board<br /> 98/2 Solai Nagar,<br /> Kamarajar Colony            <br /> Melur  Salai<br /> Sivagangai  District -630  561</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>26</p>
              </td>
              <td valign="top" width="144">
              <p>Ramanathapuram</p>
              </td>
              <td valign="top" width="192">
              <p>Ramanathapuram</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board,<br /> Ground Floor 105,<br /> 1/994-A, Sat Ibrahim Colony,<br /> Avvai Street,  <br /> Bharathi Nagar<br /> Ramanathapuram  District  -623 503</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>27</p>
              </td>
              <td valign="top" width="144">
              <p>Thiruvallur</p>
              </td>
              <td valign="top" width="192">
              <p>Thiruvallur</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board<br /> No.75 Kakkaalur  Village<br /> Plot No.13,<br /> Thiruvallur  District  -602  001</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>28</p>
              </td>
              <td valign="top" width="144">
              <p>Ariyalur .</p>
              </td>
              <td valign="top" width="192">
              <p>Ariyalur .</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board<br /> No. 15A  Kamarajar Nagar, II   street,<br /> Sendurai Road,  <br /> Ariyalur   District</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>29</p>
              </td>
              <td valign="top" width="144">
              <p>Perambalur</p>
              </td>
              <td valign="top" width="192">
              <p>Perambalur</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board<br /> No. 164,M.M. plaza, <br /> Trichy  Main  Road, <br /> Perambalur  District -1</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>30</p>
              </td>
              <td valign="top" width="144">
              <p>Kanniyakumari</p>
              </td>
              <td valign="top" width="192">
              <p>Kanniyakumari</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board<br /> 15-B  2nd Cross street,<br /> Simon Nagar,<br /> Nagercoil,<br /> Kanniyakumari District – 629 003.</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>31</p>
              </td>
              <td valign="top" width="144">
              <p>Villupuram.</p>
              </td>
              <td valign="top" width="192">
              <p>Villupuram.</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board<br /> No.27/1864, Pandian Nagar, <br /> Extn.Theivanai  Ammal College (Opp),<br /> Vela Deaf &amp; Dumb Special School (behind), <br /> Villupuram  District</p>
              </td>
              </tr>
              <tr>
              <td valign="top" width="43">
              <p>32</p>
              </td>
              <td valign="top" width="144">
              <p>Tuticorin</p>
              </td>
              <td valign="top" width="192">
              <p>Tuticorin</p>
              </td>
              <td valign="top" width="288">
              <p>Juvenile Justice Board,<br /> T.M.S.S.S Building,<br /> 176-A, 12C, Mani Nagar,<br /> Palayamkottai Road,<br /> Tuticorin, District -628 002.</p>
              </td>
              </tr>
            </tbody>
          </table>

        </div>
      </div>
    </div>
    <div class="col-md-3">
      <?php $this->load->view('pages/sidebar'); ?>
    </div>
  </div>


</div>

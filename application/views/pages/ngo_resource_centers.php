<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <section class="title">
            <h5 class="card-title pb-1 border-bottom">Resource Centers</h5>
          </section>
          <section class="page-content">
            <p> Resource Centres are functioning as a centre for providing Psycho – social support, guidance for children in need of care and protection, juvenile in conflict with law. These centres provide counselling and guidance to children and also their parents who are referred by the Child Welfare Committees, Juvenile Justice Boards and Superintendents of the Homes. The Resource Centres are run by NGO’s with the financial assistance from Government in 17 Districts in Tamil Nadu.</p>
            <p><strong>Educational Facilities:</strong></p>
            <p>In Government Children Homes, the children are provided with the facility of formal education upto 8<sup>th</sup> std. Children having aptitude for higher education are sent to Secondary Level Schools. Children interested in technical education are sent to District Industrial Training Institutes. Non-Formal education and vocational training are imparted to children who are unable to cope up with the formal education so that they could gain self-confidence and self-sustainability. The Children admitted to Government Special Homes for Boys at Chengalpattu and Government Special / Children Home for Girls, Chennai are also provided with the above Educational facilities.</p>
            <p><strong>Modernized Vocational Training:</strong></p>
            <p>Skill based vocational training such as Tailoring, Carpentry, Book binding etc are imparted to the children by the regular staff of the Institutions. Besides, the following short-term courses and trainings are being organised for children of the Institutions through voluntary agencies with financial assistance form the Government:-</p>
            <ul>
              <li>Handy Man-Plumbing / Electrical</li>
              <li>Videography / Photography</li>
              <li>Dress Making/ Tailoring/ Embroidery</li>
              <li>Fabric Painting / Book Binding</li>
              <li>Handi Craft / Artistic works/ Painting</li>
              <li>Soft toys/ Paper Bag/ Plate making/ Artificial Jewels making/ Fabric, wall and oil painting</li>
              <li>Computer DTP/ Photo shop</li>
              <li>Beautician / Mehandi Application course</li>
              <li>Food processing</li>
            </ul>
          <p class="w3-center"><b>LIST OF  RESOURCE CENTRES</b></p>
          <table class="table table-bordered" style="height: 2591px;" width="933">
          <tbody>
          <tr>
          <th width="58">
          <p align="center">Sl.No.</p>
          </th>
          <th valign="top" width="151">
          <p align="center">Name of the District</p>
          </th>
          <th width="359">
          <p align="center">Resource Centre</p>
          </th>
          </tr>
          <tr>
          <td valign="top" width="58">
          <p>1.</p>
          <p> </p>
          </td>
          <td valign="top" width="151">
          <p align="center">Chennai</p>
          <p align="center"> </p>
          </td>
          <td valign="top" width="359">
          <p>Community Health Education Society (CHES)<br /> New No.198, Old No.102-A,<br /> Rangarajapuram  Main Road, <br /> Kodambakkam, <br />Chennai 600 024.</p>
          </td>
          </tr>
          <tr>
          <td valign="top" width="58">
          <p>2.</p>
          </td>
          <td valign="top" width="151">
          <p align="center">Dharmapuri</p>
          </td>
          <td valign="top" width="359">
          <p>Hebron Caring Society for Children,<br /> Earupalli (Post),<br /> Dharmapuri 636 813.</p>
          </td>
          </tr>
          <tr>
          <td valign="top" width="58">
          <p>3.</p>
          </td>
          <td valign="top" width="151">
          <p align="center">Dindigul</p>
          </td>
          <td valign="top" width="359">
          <p>Dindigul Multipurpose Social Service Society,<br /> John Paul Complex, P.B.No.77, <br /> Nehruji Nagar,<br /> Dindigul 624 001.</p>
          </td>
          </tr>
          <tr>
          <td valign="top" width="58">
          <p>4</p>
          </td>
          <td valign="top" width="151">
          <p align="center">Erode.</p>
          </td>
          <td valign="top" width="359">
          <p>Centre For Action and Rural Education,<br /> No.6,  Kambar Street,<br /> Teachers Colony, <br /> Erode -  638 011.</p>
          </td>
          </tr>
          <tr>
          <td valign="top" width="58">
          <p>5.</p>
          </td>
          <td valign="top" width="151">
          <p align="center">Kancheepuram.</p>
          </td>
          <td valign="top" width="359">
          <p>Jeevan Ganodhaya Charitable Trust, <br /> C-46, 5th Cross Street,<br /> Anna Nagar, <br /> Chengalpet.</p>
          </td>
          </tr>
          <tr>
          <td valign="top" width="58">
          <p>6</p>
          </td>
          <td valign="top" width="151">
          <p align="center">Madurai</p>
          </td>
          <td valign="top" width="359">
          <p>M.S.Chellamuthu Trust,<br /> Research Foundation,<br /> No.643, K.K.Nagar,<br /> Madurai 625 070.</p>
          </td>
          </tr>
          <tr>
          <td valign="top" width="58">
          <p>7.</p>
          </td>
          <td valign="top" width="151">
          <p align="center">Sivagangai</p>
          </td>
          <td valign="top" width="359">
          <p>Vasantham Mahalir Munnetra Sangam,<br /> No.22, Zakir Hushain Street,<br /> Nehru Bazar,<br /> Sivagangai 630 561.</p>
          </td>
          </tr>
          <tr>
          <td valign="top" width="58">
          <p>8.</p>
          </td>
          <td valign="top" width="151">
          <p align="center">Salem</p>
          </td>
          <td valign="top" width="359">
          <p>Social Awareness and Cultural Society,<br /> No.85, Deivanayagam Pillai Street, <br /> Shevapet, <br /> Salem 636 002.</p>
          </td>
          </tr>
          <tr>
          <td valign="top" width="58">
          <p>9.</p>
          </td>
          <td valign="top" width="151">
          <p align="center">Tiruvallur</p>
          </td>
          <td valign="top" width="359">
          <p>Mass Action Network,<br /> No.14, First Floor,<br /> West Sivan Kovil Street,<br /> Vadapalani, <br /> Chennai – 26. (For Tiruvallur District)</p>
          </td>
          </tr>
          <tr>
          <td valign="top" width="58">
          <p>10.</p>
          </td>
          <td valign="top" width="151">
          <p align="center">Tiruvannamalai</p>
          </td>
          <td valign="top" width="359">
          <p>Terre-des-hommes – Core Trust,<br /> Perumbakkam Road, <br /> Tiruvannamalai  606 603.</p>
          </td>
          </tr>
          <tr>
          <td valign="top" width="58">
          <p>11.</p>
          </td>
          <td valign="top" width="151">
          <p align="center">Tirunelveli</p>
          </td>
          <td valign="top" width="359">
          <p>Navajeevan Trust,<br /> No.48-B/10, Ambai Road,<br /> Veeramanickapuram,<br /> Tirunelveli 627 005.</p>
          </td>
          </tr>
          <tr>
          <td valign="top" width="58">
          <p>12.</p>
          </td>
          <td valign="top" width="151">
          <p align="center">Virudhunagar</p>
          </td>
          <td valign="top" width="359">
          <p>Trust for Education and Social Transformation (TEST),<br /> No.23, Vazhai Kulam Single Street, <br /> Srivilliputhur 626 125.</p>
          </td>
          </tr>
          <tr>
          <td valign="top" width="58">
          <p>13.</p>
          </td>
          <td valign="top" width="151">
          <p align="center">Krishnagiri</p>
          </td>
          <td valign="top" width="359">
          <p>HELP Voluntary Organisation,<br /> Kutcherymedu,<br /> Arur,<br /> Krishnagiri– 636 903.</p>
          </td>
          </tr>
          <tr>
          <td valign="top" width="58">
          <p>14.</p>
          </td>
          <td valign="top" width="151">
          <p align="center">Theni</p>
          </td>
          <td valign="top" width="359">
          <p>Centre For Development and Communication Trust,<br /> Kamatchipuram (S.O.),<br /> Theni District 625 520.</p>
          </td>
          </tr>
          <tr>
          <td valign="top" width="58">
          <p>15</p>
          </td>
          <td valign="top" width="151">
          <p align="center">Pudukkottai</p>
          </td>
          <td valign="top" width="359">
          <p>Rural Education For Community Organisation,<br /> No.12, Chola Real Estate,<br /> Dharun Complex,<br /> Thirugokarnam Post,<br /> Pudukottai 622 002.</p>
          </td>
          </tr>
          <tr>
          <td valign="top" width="58">
          <p>16</p>
          </td>
          <td valign="top" width="151">
          <p align="center">Namakkal</p>
          </td>
          <td valign="top" width="359">
          <p>Health and Education Alternative Development Society,<br /> Opp. to Sugar Mill,<br /> Moganur,<br /> Namakkal.</p>
          </td>
          </tr>
          <tr>
          <td valign="top" width="58">
          <p>17.</p>
          </td>
          <td valign="top" width="151">
          <p align="center">Kanniyakumari</p>
          </td>
          <td valign="top" width="359">
          <p>Kottar Social Service Society,<br /> Bishop’s House,<br /> P.O.Box No.17, <br /> Nagercoil, <br /> Kanniyakumari District – 629 001.</p>
          </td>
          </tr>
          <tr>
          <td valign="top" width="58">
          <p>18</p>
          </td>
          <td valign="top" width="151">
          <p align="center">The Nilgiris</p>
          </td>
          <td valign="top" width="359">
          <p>SARAS Trust,<br /> 129-1, Bombay Castle,<br /> Near Dhamayanthi Hospital,<br /> Ootacamund – 643 001,<br /> The Nilgiris.</p>
          </td>
          </tr>
          </tbody>
          </table>
         </section>
        </div>
      </div>
    </div>
      <div class="col-md-3">
        <?php $this->load->view('pages/sidebar'); ?>
      </div>
    </div>
</div>

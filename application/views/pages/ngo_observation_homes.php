<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <section class="title">
            <h5 class="card-title pb-1 border-bottom">Observation Homes</h5>
          </section>
          <section class="page-content">
            <p>Observation Homes are meant for the temporary reception of those Juvenile in conflict with law who are not realeased on bail and their cases are pending before the Juvenile Justice Boards. At present in Tamil Nadu, there are two Observarion Homes run by Non-Governmental Organizations. The children residing in the Observation Homes are provided with basic amenities like food, clothing, shelter and bedding apart from other services like education (both formal and non-formal), vocational training, medical facility and counselling as the part of their short term rehabilitation. To ensure the safety of the children, the Government have installed surveillance and security equipments at Observation Homes in Chennai and Cuddalore.</p>
          <table class="table table-bordered">
            <tbody>
            <tr>
            <th valign="top" width="37">
            <p align="center">S.<br /> No</p>
            </th>
            <th valign="top" width="264">
            <p align="center">Name of the Institution</p>
            </th>
            <th valign="top" width="100">
            <p align="center">Age group for admission</p>
            </th>
            <th valign="top" width="102">
            <p align="center">Proposed Sanctioned Strength</p>
            </th>
            <th valign="top" width="168">
            <p align="center">Districts Covered</p>
            </th>
            </tr>
            <tr>
            <td valign="top" width="37">
            <p align="center">1.</p>
            </td>
            <td valign="top" width="264">
            <p>Observation Home for Boys and Girls<br /> under  Madurai Children  Aid  Society,<br /> No.164, Kamaraj Salai, <br /> (Kamarajar Road)<br /> Madurai – 625 009. <br /> Phone No. 0452-2626524</p>
            </td>
            <td width="100">
            <p align="center">8-18</p>
            </td>
            <td width="102">
            <p align="center">100</p>
            </td>
            <td width="168">
            <p>Madurai,<br />Ramanathapuram,<br /> Dindigul,<br /> Theni,<br />Sivagangai<br />Virudhunagar</p>
            </td>
            </tr>
            <tr>
            <td valign="top" width="37">
            <p align="center">2.</p>
            </td>
            <td valign="top" width="264">
            <p>Observation Home for Boys and Girls<br /> under Discharged Prisoner's Aid Society,<br /> No.1093 &amp;1094, Avinashi Road,<br /> Pappanaickenpalayam, <br /> Coimbatore – 641 037 Phone No.0422-2216183</p>
            </td>
            <td width="100">
            <p align="center">8-18</p>
            </td>
            <td width="102">
            <p align="center">50</p>
            </td>
            <td width="168">
            <p>Coimbatore,<br />Nilgiris,<br />Erode</p>
            </td>
            </tr>
            </tbody>
          </table>
         </section>
        </div>
      </div>
    </div>
      <div class="col-md-3">
        <?php $this->load->view('pages/sidebar'); ?>
      </div>
    </div>
</div>

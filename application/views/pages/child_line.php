<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title pb-1 border-bottom">Child Line</h5>
          <p>A child accessing service deliveries through a telephone outreach programme was launched with effect from 30.4.99 in Chennai. This project is known as "Child Line" with a simple telephone number '1098'. Any Child in distress or any person finding a child in distress situation can dial '1098' at free of cost. Childline services have been functioning in Chennai, Coimbatore, Madurai, Salem, Thirunelveli and Trichy.</p>
          <p>Collaborating Organisations with net work of NGOs attend to the phone calls round the clock and take appropriate interventions. The Scheme was initiated by Department of Social Defence and is funded by Government of India and the Department of Social Defence is the nodal Department. A Child Line Advisory Board comprising of Representatives from UNICEF, NGOs, Government of India Officials from Information Bureaux , Railways, Telecom and State Government Officials from Social Welfare, Institute of Child Health and Police have been constituted. The Commissioner of Social Defence is the Chairman of the Child Line Advisory Board.</p>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <?php $this->load->view('pages/sidebar'); ?>
    </div>
  </div>


</div>

<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <section class="title">
            <h5 class="card-title pb-1 border-bottom">CCI's Applied for Registration</h5>
          </section>
          <section class="page-content">
            <table class="table table-bordered" style="height: 219px;" width="948">
            <tbody>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Ariyalur.pdf" target="_blank" rel="noopener">Ariyalur</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Chennai.pdf" target="_blank" rel="noopener">Chennai</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Coimbatore.pdf" target="_blank" rel="noopener">Coimbatore</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Cuddalore.pdf" target="_blank" rel="noopener">Cuddalore</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Dharmapuri.pdf" target="_blank" rel="noopener">Dharmapurai</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Dindigul.pdf" target="_blank" rel="noopener">Dindigul</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Erode.pdf" target="_blank" rel="noopener">Erode</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Kancheepuram.pdf" target="_blank" rel="noopener">Kanchipuram</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Kanniyakumari.pdf" target="_blank" rel="noopener">Kanyakumari</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Karur.pdf" target="_blank" rel="noopener">Karur</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Krishnagiri.pdf" target="_blank" rel="noopener">krishnagiri</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Madurai.pdf" target="_blank" rel="noopener">Madurai</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Nagapattinam.pdf" target="_blank" rel="noopener">Nagapattinam</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Namakkal.pdf" target="_blank" rel="noopener">Namakkal</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Perambalur.pdf" target="_blank" rel="noopener">Perambalur</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Pudukkottai.pdf" target="_blank" rel="noopener">Pudukkottai</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Ramanathapuram.pdf" target="_blank" rel="noopener">Ramanathapuram</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Salem.pdf" target="_blank" rel="noopener">Salem</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Sivagangai.pdf" target="_blank" rel="noopener">Sivaganga</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Thanjavur.pdf" target="_blank" rel="noopener">Tanjavur</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Nilgiris.pdf" target="_blank" rel="noopener">The Nilgiris</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Theni.pdf" target="_blank" rel="noopener">Theni</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Thiruvallur.pdf" target="_blank" rel="noopener">Thiruvallur</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Thiruvarur.pdf" target="_blank" rel="noopener">Thiruvarur</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Trichy.pdf" target="_blank" rel="noopener">Tiruchirappalli</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Thirunelveli.pdf" target="_blank" rel="noopener">Tirunelveli</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Tiruppur.pdf" target="_blank" rel="noopener">Tiruppur</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Thiruvannamalai.pdf" target="_blank" rel="noopener">Tiruvannamalai</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Thoothukudi.pdf" target="_blank" rel="noopener">Thoothukkudi</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Vellore.pdf" target="_blank" rel="noopener">Vellore</a></td>
            </tr>
            <tr>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Villupuram.pdf" target="_blank" rel="noopener">Villupuram</a></td>
            <td><a href="<?= base_url() ?>documents/registration/applied-for-registration/Virudhunagar.pdf" target="_blank" rel="noopener">Virudhunagar</a></td>
            <td></td>
            </tr>
            </tbody>
            </table>
          </section>
        </div>
      </div>
    </div>
      <div class="col-md-3">
        <?php $this->load->view('pages/sidebar'); ?>
      </div>
    </div>
</div>

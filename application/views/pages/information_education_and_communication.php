<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <section class="title">
            <h5 class="card-title pb-1 border-bottom">Information Education & Communication</h5>
          </section>
          <section class="page-content">
            <table class="table table-bordered" style="height: 139px;" width="936">
              <tbody>
              <tr>
                <td><a href="<?= base_url()?>documents/IEC/Handling%20behavioural%20problems%20among%20children.pdf" target="_blank" rel="noopener">Handling behavioural problems among children</a></td>
              </tr>
              <tr>
                <td><a href="<?= base_url()?>documents/IEC/Holistic%20Child%20Development%20-%20Where%20and%20How.pdf" target="_blank" rel="noopener">Holistic Child Development - Where and How</a></td>
              </tr>
              <tr>
                <td><a href="<?= base_url()?>documents/IEC/Migration%20and%20its%20impact%20on%20Children.pdf" target="_blank" rel="noopener">Migration and its impact on Children</a></td>
              </tr>
              <tr>
                <td><a href="<?= base_url()?>documents/IEC/POCSO%202012%20-%20Offences%20and%20Penalties.pdf" target="_blank" rel="noopener">POCSO 2012 - Offences and Penalties</a></td>
              </tr>
              <tr>
                <td><a href="<?= base_url()?>documents/IEC/Roles%20and%20Responsibilities%20of%20VLCPC%20&amp;%20BLCPC%20Book%201%20Final.pdf" target="_blank" rel="noopener">Roles and Responsibilities of VLCPC &amp; BLCPC Book 1 Final</a></td>
              </tr>
              </tbody>
            </table>
          </section>
        </div>
      </div>
    </div>
      <div class="col-md-3">
        <?php $this->load->view('pages/sidebar'); ?>
      </div>
    </div>
</div>

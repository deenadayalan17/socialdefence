<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title pb-1 border-bottom">Swadhar Scheme</h5>
          <p class="maincontent" align="justify">It is a scheme for women in difficult circumstances, being implemented by the Department of Women and Child Development, Ministry of Human Resource Development, Government of India.</p>
          <p align="justify"><strong>Objectives of the Scheme</strong></p>
          <table class="table table-bordered">
            <tbody>
              <tr>
                <td>[1]</td>
                <td>To provide primary need of shelter, food, clothing and care to the marginalized women/girls living in difficult circumstances who are without any social and economic support;</td>
              </tr>
              <tr>
                <td>[2]</td>
                <td>To provide emotional support and counseling to such women; To provide emotional support and counseling to such women;</td>
              </tr>
              <tr>
                <td>[3]</td>
                <td>To rehabilitate them socially and economically through education, awareness, skill up-gradation and personality development through behaviourial training etc.,</td>
              </tr>
              <tr>
                <td>[4]</td>
                <td>To arrange for specific clinical, legal and other support for women/girls in need of the intervention by linking and networking with other organizations in both Government and non-Government sector on case-to-case basis;</td>
              </tr>
              <tr>
                <td>[5]</td>
                <td>To provide for help line or other facilities to such women in distress; and</td>
              </tr>
              <tr>
                <td>[6]</td>
                <td>To provide such other services as will be required for the support and rehabilitation of such women in distress.</td>
              </tr>
            </tbody>
          </table>
          <p><strong>Target Group / Beneficiaries</strong></p>
          <table class="table table-bordered">
            <tbody>
              <tr>
                <td>[1]</td>
                <td>Widows deserted by their families and relatives and left uncared near religious places where they are victims of exploitation.</td>
              </tr>
              <tr>
                <td>[2]</td>
                <td>Women prisoners released from jail and without family support</td>
              </tr>
              <tr>
                <td>[3]</td>
                <td>Women survivors of natural disaster who have been rendered homeless and are without any social and economic support.</td>
              </tr>
              <tr>
                <td>[4]</td>
                <td>Trafficked women/girls rescued or runaway from brothels or other places or women/girl victims of sexual crimes who are disowned by family or who do not want to go back to respective family for various reasons.</td>
              </tr>
              <tr>
                <td>[5]</td>
                <td>Women victims of terrorist / extremist violence who are without any family support and without any economic means for survival.</td>
              </tr>
              <tr>
                <td>[6]</td>
                <td>Mentally challenged women (except for the psychotic categories who require care in specialized environment in mental hospitals) who are without any support of family or relatives.</td>
              </tr>
              <tr>
                <td>[7]</td>
                <td>Women with HIV/AIDS deserted by their family or women who have lost their husband due to HIV/AIDS and are without social/economic support; or</td>
              </tr>
              <tr>
                <td>[8]</td>
                <td>Similarly placed women in difficult circumstances.</td>
              </tr>
            </tbody>
          </table>
          <p><strong>Implementing Agencies:</strong><br> The implementing agencies can be Voluntary Organisations who are willing to take up the responsibility of rehabilitating such women. The organization must have adequate experience and expertise of taking up such works of rehabilitation.</p>
          <p>[For Guidelines please log on to (<a href="http://www.wcd.nic.in/" target="_blank" rel="noopener">www.wcd.nic.in</a>)]</p>
          <p><strong>Addresses of existing implementing organizations: </strong></p>
          <table class="table table-bordered">
            <tbody>
              <tr>
                <th>Sl. No.</th>
                <th>Name and address of the Organisation running the Swadhar Home</th>
                <th>Address of the Swadhar Home</th>
              </tr>
              <tr>
                <td>1</td>
                <td>Avvai Village Welfare Society,<br> 260, Public Office Road,<br> Vellipalayam, Nagapattinam,<br> Nagapattinam District.</td>
                <td>58, New Street,Anna Square, Nagapattinam.</td>
              </tr>
              <tr>
                <td>2</td>
                <td>The Banyan 6th Main Road,<br> Mogappair Eri Scheme,<br> Chennai – 600 058.</td>
                <td>The Banyan,6th Main Road, Mogappair Eri Scheme, Chennai – 600 058.</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Kalaiselvi Karunalaya Social Service Society,<br> PP1, 3rd Block,<br> Magappair West, Chennai.</td>
                <td>Shubhiksha,5/286, M.G.R. Street,Vijayanagaram, Santhoshapuram, Medavakkam, Kancheepuram District.</td>
              </tr>
              <tr>
                <td>4</td>
                <td>Kovai Auxilium Salesian Sisters Society,<br>8/1/E/122, Kasthuribai Street,<br> Anna Nagar, Ammapalayam, <br>Annuparpalayam Post, Tirupur, <br>Coimbatore District.</td>
                <td>8/1/E/122, Kasthuribai Street, Anna Nagar, Ammapalayam, Annuparpalayam Post, Tirupur, Coimbatore District.</td>
              </tr>
              <tr>
                <td>5</td>
                <td>Maitri Socieity, Koduvillarpatti,<br> Theni District.</td>
                <td>Koduvillarpatti, Theni District.</td>
              </tr>
              <tr>
                <td>6</td>
                <td>Mass Action Network India Trust,<br> No.14, First Floor, West Sivan Koil Street, <br>Vadapalani, Chennai – 600 026.</td>
                <td>No.138, Second Main Road, Ashtalakshmi Nagar, Athalapakkam, Chennai - 600 116.</td>
              </tr>
              <tr>
                <td>7</td>
                <td>Rural Education for Action and Development,<br>H27, 5th Cross, Second Main Road, <br>R.M.Colony, Dindigul,<br>Dindigul District.</td>
                <td>No.3/5.A, V.O.C.Street, Railway Feeder Road, Palani, Dindigul District.</td>
              </tr>
              <tr>
                <td>8</td>
                <td>Sri Venkateswara Orphanage,<br> Kaduvelli,<br> Thilaisthanam Post,<br>Thanjavur District.</td>
                <td>No.2/20, Kaduvelli, Thilaisthanam Post, Thanjavur District.</td>
              </tr>
              <tr>
                <td>9</td>
                <td>Vinmathee Educational and Rural Development Society,<br> Trichy Main Road,<br> Manaparai, <br>Tiruchirapalli District.</td>
                <td>Trichy Road, Manaparai, Tiruchirapalli District.</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <?php $this->load->view('pages/sidebar'); ?>
    </div>
  </div>


</div>

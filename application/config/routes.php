<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'pages/views';
$route['organization/organization-structure']='pages/views/organization_structure';
$route['organization/history']='pages/views/history';
$route['organization/administrative-setup']='pages/views/administrative_setup';
$route['citizen-charter']='pages/views/citizen_charter';
$route['schemes-and-services/ujjawala-scheme']='pages/views/ujjawala_scheme';
$route['schemes-and-services/swadhar-scheme']='pages/views/swadhar_scheme';
$route['schemes-and-services/child-line']='pages/views/child_line';
$route['schemes-and-services/icps/institutional-services']='pages/views/institutional_services';
$route['schemes-and-services/icps/non-institutional-services/sponsership-and-foster-care']='pages/views/sponsership_and_foster_care';
$route['schemes-and-services/icps/non-institutional-services/adoption']='pages/views/adoption';
$route['schemes-and-services/icps/non-institutional-services/after-care']='pages/views/after_care';
$route['schemes-and-services/de-addiction-program']='pages/views/de_addiction_program';
$route['documents/acts-and-rules']='pages/views/acts_rules';
$route['documents/government-orders']='pages/views/government_orders';
$route['documents/policies']='pages/views/policies';
$route['documents/downloads']='pages/views/downloads';
$route['documents/notifications']='pages/views/notifications';
$route['institutions/juvenile-justice-boards']='pages/views/juvenile_justice_boards';
$route['institutions/child-welfare-committee']='pages/views/child_welfare_committee';
$route['institutions/government/children-homes']='pages/views/government_child_homes';
$route['institutions/government/observation-homes']='pages/views/government_observation_homes';
$route['institutions/government/after-care-institutions']='pages/views/government_after_care_institutions';
$route['institutions/government/special-homes']='pages/views/government_special_homes';
$route['institutions/government/reception-units']='pages/views/government_reception_units';
$route['institutions/ngo/children-homes']='pages/views/ngo_children_homes';
$route['institutions/ngo/observation-homes']='pages/views/ngo_observation_homes';
$route['institutions/ngo/resource-centers']='pages/views/ngo_resource_centers';
$route['institutions/ngo/reception-units']='pages/views/ngo_reception_units';
$route['institutions.ngo/de-addiction-centers']='pages/views/de_addiction_centers';
$route['institutions/registration/registered']='pages/views/registered_ccis';
$route['institutions/registration/applied-for-renewal']='pages/views/applied_for_renewal';
$route['institutions/registration/not-applied-for-renewal']='pages/views/not_applied_for_renewal';
$route['institutions/registration/applied-for-registration']='pages/views/applied_for_registration';
$route['institutions/registration/not-applied-for-registration']='pages/views/not_applied_for_registration';
$route['publications/annual-reports']='pages/views/annual_reports';
$route['publications/policy-notes']='pages/views/policy_notes';
$route['publications/information-education-and-communication']='pages/views/information_education_and_communication';
$route['contacts/state-child-protection-society']='pages/views/state_child_protection_society';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
